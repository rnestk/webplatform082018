
DELETE [dbo].[AdmMenuItem]

declare @imageurl nvarchar(max) 
declare @routeurl nvarchar(max)
declare @parentid int
declare @title nvarchar(max)
declare @description nvarchar(max)
declare @authorId NVARCHAR(128)

IF(NOT EXISTS(SELECT TOP 1 1 FROM [AdmMenuItem] WHERE [Title] = N'Administracja' AND [RouteUrl] IS NULL))
BEGIN
set @imageurl = N'/Images/Menu/8.jpg'
set @routeurl = null
set @parentid = null
set @title = N'Administracja'
set @description = null
SET @authorId = (SELECT TOP 1 Id FROM AdmUser WHERE UserName = N'SYSTEM')

INSERT INTO [dbo].[AdmMenuItem]
([ParentId],[ImageUrl],[RouteUrl],[Title],[Description],[DateCreated],[DateModified],[IsActive],[IsRemoved],[RowGuid],[AuthorId])
VALUES
(@parentid,@imageurl,@routeurl,@title,@description,GETDATE(),NULL,1,0,NEWID(),@authorId)

set @parentid = SCOPE_IDENTITY()
END
-- ----------------------------------------- UŻYTKOWNICY  --------------------------------------------------

DECLARE @uzytkownicyParentId INT

IF(NOT EXISTS(SELECT TOP 1 1 FROM [AdmMenuItem] WHERE [Title] = N'Użytkownicy'))
BEGIN
set @imageurl = null
set @routeurl = NULL
set @title = N'Użytkownicy'
set @description = null

INSERT INTO [dbo].[AdmMenuItem]
([ParentId],[ImageUrl],[RouteUrl],[Title],[Description],[DateCreated],[DateModified],[IsActive],[IsRemoved],[RowGuid],[AuthorId])
VALUES
(@parentid,@imageurl,@routeurl,@title,@description,GETDATE(),NULL,1,0,NEWID(), @authorId)
END

SET @uzytkownicyParentId = SCOPE_IDENTITY()

-- ----------------------------------------- UŻYTKOWNICY AKTYWNI --------------------------------------------------


IF(NOT EXISTS(SELECT TOP 1 1 FROM [AdmMenuItem] WHERE [RouteUrl] = N'uzytkownicy-aktywni-lista'))
BEGIN
set @imageurl = null
set @routeurl = N'uzytkownicy-aktywni-lista'
set @title = N'Użytkownicy aktywni'
set @description = null

INSERT INTO [dbo].[AdmMenuItem]
([ParentId],[ImageUrl],[RouteUrl],[Title],[Description],[DateCreated],[DateModified],[IsActive],[IsRemoved],[RowGuid],[AuthorId])
VALUES
(@uzytkownicyParentId,@imageurl,@routeurl,@title,@description,GETDATE(),NULL,1,0,NEWID(), @authorId)
END


-- ----------------------------------------- UŻYTKOWNICY NIEAKTYWNI --------------------------------------------------

IF(NOT EXISTS(SELECT TOP 1 1 FROM [AdmMenuItem] WHERE [RouteUrl] = N'uzytkownicy-nieaktywni-lista'))
BEGIN
set @imageurl = null
set @routeurl = N'uzytkownicy-nieaktywni-lista'
set @title = N'Użytkownicy nieaktywni'
set @description = null

INSERT INTO [dbo].[AdmMenuItem]
([ParentId],[ImageUrl],[RouteUrl],[Title],[Description],[DateCreated],[DateModified],[IsActive],[IsRemoved],[RowGuid],[AuthorId])
VALUES
(@uzytkownicyParentId,@imageurl,@routeurl,@title,@description,GETDATE(),NULL,1,0,NEWID(), @authorId)
END


-- ----------------------------------------- ZASOBY --------------------------------------------------

IF(NOT EXISTS(SELECT TOP 1 1 FROM [AdmMenuItem] WHERE [RouteUrl] = N'zasob-lista'))
BEGIN
set @imageurl = null
set @routeurl = N'zasob-lista'
set @title = N'Zasoby aplikacji'
set @description = null

INSERT INTO [dbo].[AdmMenuItem]
([ParentId],[ImageUrl],[RouteUrl],[Title],[Description],[DateCreated],[DateModified],[IsActive],[IsRemoved],[RowGuid],[AuthorId])
VALUES
(@parentid,@imageurl,@routeurl,@title,@description,GETDATE(),NULL,1,0,NEWID(), @authorId)
END



-- ----------------------------------------- ROlE --------------------------------------------------

IF(NOT EXISTS(SELECT TOP 1 1 FROM [AdmMenuItem] WHERE [RouteUrl] = N'role-lista'))
BEGIN
set @imageurl = null
set @routeurl = N'role-lista'
set @title = N'Role aplikacji'
set @description = null

INSERT INTO [dbo].[AdmMenuItem]
([ParentId],[ImageUrl],[RouteUrl],[Title],[Description],[DateCreated],[DateModified],[IsActive],[IsRemoved],[RowGuid],[AuthorId])
VALUES
(@parentid,@imageurl,@routeurl,@title,@description,GETDATE(),NULL,1,0,NEWID(), @authorId)



END


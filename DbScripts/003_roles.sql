
DECLARE @authorId NVARCHAR(128) = (SELECT Id FROM AdmUser WHERE UserName = N'SYSTEM')

IF (NOT EXISTS(SELECT TOP 1 1 FROM AdmRole WHERE Name = N'Administrator'))
INSERT INTO [dbo].[AdmRole]
([Name],[Description],[DateCreated],[DateModified],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[DisplayName])
VALUES
(
N'Administrator',N'Rola administratora',GETDATE(),NULL,1,0,NEWID(),@authorId,N'Administrator'
)

IF (NOT EXISTS(SELECT TOP 1 1 FROM AdmRole WHERE Name = N'User'))
INSERT INTO [dbo].[AdmRole]
([Name],[Description],[DateCreated],[DateModified],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[DisplayName])
VALUES
(
N'User',N'Rola użytkownika',GETDATE(),NULL,1,0,NEWID(),@authorId,N'Użytkownik'
)


IF (NOT EXISTS(SELECT TOP 1 1 FROM AdmRole WHERE Name = N'Student'))
INSERT INTO [dbo].[AdmRole]
([Name],[Description],[DateCreated],[DateModified],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[DisplayName])
VALUES
(
N'Student',N'Rola użytkownika',GETDATE(),NULL,1,0,NEWID(),@authorId,N'Student'
)

IF (NOT EXISTS(SELECT TOP 1 1 FROM AdmRole WHERE Name = N'Weryfikator'))
INSERT INTO [dbo].[AdmRole]
([Name],[Description],[DateCreated],[DateModified],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[DisplayName])
VALUES
(
N'Weryfikator',N'Rola użytkownika',GETDATE(),NULL,1,0,NEWID(),@authorId,N'Weryfikator'
)






DECLARE @authorId NVARCHAR(128) = (SELECT Id FROM AdmUser WHERE UserName = N'SYSTEM')

-- ----------------------------------------------- Zasoby -----------------------------------------------------------------------

IF (NOT EXISTS(SELECT TOP 1 1 FROM [AdmResource] WHERE Symbol = N'Resource_List'))
BEGIN
INSERT INTO [dbo].[AdmResource]
([Area],[Controller],[Action],[Description],[DateCreated],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[Symbol])
VALUES
(NULL, N'Resource', N'List', N'Lista zasob�w', GETDATE(), 1, 0, NEWID(), @authorId, N'Resource_List')
END


IF (NOT EXISTS(SELECT TOP 1 1 FROM [AdmResource] WHERE Symbol = N'Resource_Add'))
BEGIN
INSERT INTO [dbo].[AdmResource]
([Area],[Controller],[Action],[Description],[DateCreated],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[Symbol])
VALUES
(NULL, N'Resource', N'Add', N'Dodawanie zasobu', GETDATE(), 1, 0, NEWID(), @authorId, N'Resource_Add')
END

IF (NOT EXISTS(SELECT TOP 1 1 FROM [AdmResource] WHERE Symbol = N'Resource_Edit'))
BEGIN
INSERT INTO [dbo].[AdmResource]
([Area],[Controller],[Action],[Description],[DateCreated],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[Symbol])
VALUES
(NULL, N'Resource', N'Edit', N'Edycja zasobu', GETDATE(), 1, 0, NEWID(), @authorId, N'Resource_Edit')
END


IF (NOT EXISTS(SELECT TOP 1 1 FROM [AdmResource] WHERE Symbol = N'Resource_Delete'))
BEGIN
INSERT INTO [dbo].[AdmResource]
([Area],[Controller],[Action],[Description],[DateCreated],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[Symbol])
VALUES
(NULL, N'Resource', N'Delete', N'Usuwanie zasobu', GETDATE(), 1, 0, NEWID(), @authorId, N'Resource_Delete')
END

-- ------------------------------------------------ U�ytkownicy ----------------------------------------------------------------------

IF (NOT EXISTS(SELECT TOP 1 1 FROM [AdmResource] WHERE Symbol = N'User_List'))
BEGIN
INSERT INTO [dbo].[AdmResource]
([Area],[Controller],[Action],[Description],[DateCreated],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[Symbol])
VALUES
(NULL, N'User', N'List', N'Lista u�ytkownik�w', GETDATE(), 1, 0, NEWID(), @authorId, N'User_List')
END


IF (NOT EXISTS(SELECT TOP 1 1 FROM [AdmResource] WHERE Symbol = N'User_Add'))
BEGIN
INSERT INTO [dbo].[AdmResource]
([Area],[Controller],[Action],[Description],[DateCreated],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[Symbol])
VALUES
(NULL, N'User', N'Add', N'Dodawanie u�ytkownika', GETDATE(), 1, 0, NEWID(), @authorId, N'User_Add')
END

IF (NOT EXISTS(SELECT TOP 1 1 FROM [AdmResource] WHERE Symbol = N'User_Edit'))
BEGIN
INSERT INTO [dbo].[AdmResource]
([Area],[Controller],[Action],[Description],[DateCreated],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[Symbol])
VALUES
(NULL, N'User', N'Edit', N'Edycja u�ytkownika', GETDATE(), 1, 0, NEWID(), @authorId, N'User_Edit')
END


IF (NOT EXISTS(SELECT TOP 1 1 FROM [AdmResource] WHERE Symbol = N'User_Delete'))
BEGIN
INSERT INTO [dbo].[AdmResource]
([Area],[Controller],[Action],[Description],[DateCreated],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[Symbol])
VALUES
(NULL, N'User', N'Delete', N'Usuwanie u�ytkownika', GETDATE(), 1, 0, NEWID(), @authorId, N'User_Delete')
END

-- ------------------------------------------------ Role ----------------------------------------------------------------------

IF (NOT EXISTS(SELECT TOP 1 1 FROM [AdmResource] WHERE Symbol = N'Role_List'))
BEGIN
INSERT INTO [dbo].[AdmResource]
([Area],[Controller],[Action],[Description],[DateCreated],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[Symbol])
VALUES
(NULL, N'Role', N'List', N'Lista r�l', GETDATE(), 1, 0, NEWID(), @authorId, N'Role_List')
END


IF (NOT EXISTS(SELECT TOP 1 1 FROM [AdmResource] WHERE Symbol = N'Role_Add'))
BEGIN
INSERT INTO [dbo].[AdmResource]
([Area],[Controller],[Action],[Description],[DateCreated],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[Symbol])
VALUES
(NULL, N'Role', N'Add', N'Dodawanie roli', GETDATE(), 1, 0, NEWID(), @authorId, N'Role_Add')
END

IF (NOT EXISTS(SELECT TOP 1 1 FROM [AdmResource] WHERE Symbol = N'Role_Edit'))
BEGIN
INSERT INTO [dbo].[AdmResource]
([Area],[Controller],[Action],[Description],[DateCreated],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[Symbol])
VALUES
(NULL, N'Role', N'Edit', N'Edycja roli', GETDATE(), 1, 0, NEWID(), @authorId, N'Role_Edit')
END


IF (NOT EXISTS(SELECT TOP 1 1 FROM [AdmResource] WHERE Symbol = N'Role_Delete'))
BEGIN
INSERT INTO [dbo].[AdmResource]
([Area],[Controller],[Action],[Description],[DateCreated],[IsActive],[IsRemoved],[RowGuid],[AuthorId],[Symbol])
VALUES
(NULL, N'Role', N'Delete', N'Usuwanie roli', GETDATE(), 1, 0, NEWID(), @authorId, N'Role_Delete')
END




﻿CREATE TABLE [dbo].[AdmResource] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Area]         NVARCHAR (MAX) NULL,
    [Controller]   NVARCHAR (MAX) NOT NULL,
    [Action]       NVARCHAR (MAX) NOT NULL,
    [Description]  NVARCHAR (MAX) NOT NULL,
    [DateCreated]  DATETIME       NOT NULL,
    [DateModified] DATETIME       NULL,
    [IsActive]     BIT            NOT NULL,
    [IsRemoved]    BIT            NOT NULL,
    [RowGuid]      NVARCHAR (MAX) NOT NULL,
    [AuthorId]     NVARCHAR (MAX) NOT NULL,
    [Symbol] NVARCHAR(MAX) NOT NULL, 
    CONSTRAINT [PK_dbo.AdmResource] PRIMARY KEY CLUSTERED ([Id] ASC) 
);


GO

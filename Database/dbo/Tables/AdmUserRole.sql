﻿CREATE TABLE [dbo].[AdmUserRole] (
    [RoleId]       INT            NOT NULL,
    [UserId]       NVARCHAR (128) NOT NULL,
    [DateCreated]  DATETIME       NOT NULL,
    [DateModified] DATETIME       NULL,
    [IsActive]     BIT            NOT NULL,
    [IsRemoved]    BIT            NOT NULL,
    [RowGuid]      NVARCHAR (MAX) NULL,
    [AuthorId]     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.AdmUserRole] PRIMARY KEY CLUSTERED ([RoleId] ASC, [UserId] ASC),
    CONSTRAINT [FK_dbo.AdmUserRole_dbo.AdmRole_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[AdmRole] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.AdmUserRole_dbo.AdmUser_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AdmUser] ([Id]) ON DELETE CASCADE
);




GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[AdmUserRole]([UserId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RoleId]
    ON [dbo].[AdmUserRole]([RoleId] ASC);


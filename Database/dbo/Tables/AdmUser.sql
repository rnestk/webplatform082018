﻿CREATE TABLE [dbo].[AdmUser] (
    [Id]                   NVARCHAR (128) NOT NULL,
    [DateCreated]          DATETIME       NOT NULL,
    [DateModified]         DATETIME       NULL,
    [IsActive]             BIT            NOT NULL,
    [IsRemoved]            BIT            NOT NULL,
    [RowGuid]              NVARCHAR (MAX) NULL,
    [AuthorId]             NVARCHAR (MAX) NULL,
    [Email]                NVARCHAR (256) NULL,
    [EmailConfirmed]       BIT            NOT NULL,
    [PasswordHash]         NVARCHAR (MAX) NULL,
    [SecurityStamp]        NVARCHAR (MAX) NULL,
    [PhoneNumber]          NVARCHAR (MAX) NULL,
    [PhoneNumberConfirmed] BIT            NOT NULL,
    [TwoFactorEnabled]     BIT            NOT NULL,
    [LockoutEndDateUtc]    DATETIME       NULL,
    [LockoutEnabled]       BIT            NOT NULL,
    [AccessFailedCount]    INT            NOT NULL,
    [UserName]             NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_dbo.AdmUser] PRIMARY KEY CLUSTERED ([Id] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex]
    ON [dbo].[AdmUser]([UserName] ASC);


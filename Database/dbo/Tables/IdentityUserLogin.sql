﻿CREATE TABLE [dbo].[IdentityUserLogin] (
    [LoginProvider] NVARCHAR (128) NOT NULL,
    [ProviderKey]   NVARCHAR (128) NOT NULL,
    [UserId]        NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_dbo.IdentityUserLogin] PRIMARY KEY CLUSTERED ([LoginProvider] ASC, [ProviderKey] ASC, [UserId] ASC),
    CONSTRAINT [FK_dbo.IdentityUserLogin_dbo.AdmUser_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AdmUser] ([Id]) ON DELETE CASCADE
);




GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[IdentityUserLogin]([UserId] ASC);


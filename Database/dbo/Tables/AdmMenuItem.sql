﻿CREATE TABLE [dbo].[AdmMenuItem] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [ParentId]     INT            NULL,
    [ImageUrl]     NVARCHAR (MAX) NULL,
    [RouteUrl]     NVARCHAR (MAX) NULL,
    [Title]        NVARCHAR (MAX) NOT NULL,
    [Description]  NVARCHAR (MAX) NULL,
    [DateCreated]  DATETIME       NOT NULL,
    [DateModified] DATETIME       NULL,
    [IsActive]     BIT            NOT NULL,
    [IsRemoved]    BIT            NOT NULL,
    [RowGuid]      NVARCHAR (MAX) NULL,
    [AuthorId]     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.AdmMenuItem] PRIMARY KEY CLUSTERED ([Id] ASC)
);


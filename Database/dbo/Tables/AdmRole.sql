﻿CREATE TABLE [dbo].[AdmRole] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (MAX) NOT NULL,
    [Description]  NVARCHAR (MAX) NULL,
    [DateCreated]  DATETIME       NOT NULL,
    [DateModified] DATETIME       NULL,
    [IsActive]     BIT            NOT NULL,
    [IsRemoved]    BIT            NOT NULL,
    [RowGuid]      NVARCHAR (MAX) NULL,
    [AuthorId]     NVARCHAR (MAX) NULL,
    [DisplayName] NVARCHAR(MAX) NOT NULL, 
    CONSTRAINT [PK_dbo.AdmRole] PRIMARY KEY CLUSTERED ([Id] ASC)
);


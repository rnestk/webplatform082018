﻿CREATE TABLE [dbo].[AdmRoleResource] (
    [RoleId]       INT            NOT NULL,
    [ResourceId]   INT            NOT NULL,
    [DateCreated]  DATETIME       NOT NULL,
    [DateModified] DATETIME       NULL,
    [IsActive]     BIT            NOT NULL,
    [IsRemoved]    BIT            NOT NULL,
    [RowGuid]      NVARCHAR (MAX) NOT NULL,
    [AuthorId]     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.AdmRoleResource] PRIMARY KEY CLUSTERED ([RoleId] ASC, [ResourceId] ASC),
    CONSTRAINT [FK_dbo.AdmRoleResource_dbo.AdmResource_ResourceId] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[AdmResource] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.AdmRoleResource_dbo.AdmRole_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[AdmRole] ([Id]) ON DELETE CASCADE
);




GO
CREATE NONCLUSTERED INDEX [IX_RoleId]
    ON [dbo].[AdmRoleResource]([RoleId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ResourceId]
    ON [dbo].[AdmRoleResource]([ResourceId] ASC);


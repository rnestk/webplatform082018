﻿CREATE TABLE [dbo].[IdentityUserRole] (
    [UserId] NVARCHAR (128) NOT NULL,
    [RoleId] NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_dbo.IdentityUserRole] PRIMARY KEY CLUSTERED ([UserId] ASC, [RoleId] ASC),
    CONSTRAINT [FK_dbo.IdentityUserRole_dbo.AdmUser_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AdmUser] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.IdentityUserRole_dbo.IdentityRole_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[IdentityRole] ([Id]) ON DELETE CASCADE
);




GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[IdentityUserRole]([UserId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RoleId]
    ON [dbo].[IdentityUserRole]([RoleId] ASC);


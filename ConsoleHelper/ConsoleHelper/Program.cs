﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.Entity;
using ConsoleHelper.Entities;
using System.Security.Cryptography;

namespace ConsoleHelper
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("START");

            AddUsers();

            Console.WriteLine("END");
            Console.Read();
        }


        static void AddUsers()
        {
            string cs = @"Data Source=DESKTOP-CGD1C99\SQLEXPRESS;Initial Catalog=WebPlatform;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            var people = new List<Person>();

            using (var ctx = new Adv())
            {
                ctx.Database.CommandTimeout = 360;
                people = (
                    from p in ctx.Person.Include(e => e.EmailAddress)
                    group p by p.LastName
                    into gr
                    orderby Guid.NewGuid()
                    select gr.FirstOrDefault()
                         )
                         .Take(500)
                         .ToList();

                using (var connection = new SqlConnection())
                {
                    connection.ConnectionString = cs;
                    connection.Open();

                    foreach (var person in people)
                    {
                        var bytes = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes("test123"));
                        using (var command = new SqlCommand())
                        {
                            command.Connection = connection;
                            command.CommandType = System.Data.CommandType.Text;

                            command.CommandText =
                                    @"INSERT INTO AdmUser 
                                    (Id, DateCreated, IsActive, IsRemoved, RowGuid, AuthorId, Email, EmailConfirmed, PasswordHash, SecurityStamp,
                                    PhoneNumberConfirmed, TwoFactorEnabled, LockoutEnabled, AccessFailedCount, UserName)
                                    VALUES
                                    (@Id, @DateCreated, @IsActive, @IsRemoved, @RowGuid, @AuthorId, @Email, @EmailConfirmed,
                                    @PasswordHash, @SecurityStamp, @PhoneNumberConfirmed, @TwoFactorEnabled, @LockoutEnabled, @AccessFailedCount, @UserName)";
                            command.Parameters.AddWithValue("@Id", Guid.NewGuid().ToString());
                            command.Parameters.AddWithValue("@DateCreated", DateTime.Now.ToString());
                            command.Parameters.AddWithValue("@IsActive", true);
                            command.Parameters.AddWithValue("@IsRemoved", false);
                            command.Parameters.AddWithValue("@RowGuid", Guid.NewGuid().ToString());
                            command.Parameters.AddWithValue("@AuthorId", Guid.Empty.ToString());
                            command.Parameters.AddWithValue("@Email", person.EmailAddress?.FirstOrDefault()?.EmailAddress1);
                            command.Parameters.AddWithValue("@EmailConfirmed", false);
                            command.Parameters.AddWithValue("@PasswordHash", Encoding.UTF8.GetString(bytes));
                            command.Parameters.AddWithValue("@SecurityStamp", Guid.NewGuid().ToString("D"));
                            command.Parameters.AddWithValue("@PhoneNumberConfirmed", false);
                            command.Parameters.AddWithValue("@TwoFactorEnabled", false);
                            command.Parameters.AddWithValue("@LockoutEnabled", true);
                            command.Parameters.AddWithValue("@AccessFailedCount", 0);
                            command.Parameters.AddWithValue("@UserName", string.Format("{0}.{1}", person.LastName, person.FirstName));
                            command.ExecuteNonQuery();
                            command.Parameters.Clear();
                        }

                    }

                }



            }

            

          
        }

    }
}

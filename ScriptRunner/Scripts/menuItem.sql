
DELETE [dbo].[AdmMenuItem]

declare @imageurl nvarchar(max) 
declare @routeurl nvarchar(max)
declare @parentid int
declare @title nvarchar(max)
declare @description nvarchar(max)

set @imageurl = N'/Images/Menu/8.jpg'
set @routeurl = null
set @parentid = null
set @title = N'Administracja'
set @description = null

INSERT INTO [dbo].[AdmMenuItem]
([ParentId],[ImageUrl],[RouteUrl],[Title],[Description],[DateCreated],[DateModified],[IsActive],[IsRemoved],[RowGuid],[AuthorId])
VALUES
(@parentid,@imageurl,@routeurl,@title,@description,GETDATE(),NULL,1,0,NEWID(),NEWID())

set @parentid = SCOPE_IDENTITY()

set @imageurl = null
set @routeurl = N'uzytkownicy-aktywni-lista'
set @title = N'Użytkownicy aktywni'
set @description = null

INSERT INTO [dbo].[AdmMenuItem]
([ParentId],[ImageUrl],[RouteUrl],[Title],[Description],[DateCreated],[DateModified],[IsActive],[IsRemoved],[RowGuid],[AuthorId])
VALUES
(@parentid,@imageurl,@routeurl,@title,@description,GETDATE(),NULL,1,0,NEWID(),NEWID())

-- -------------------------------------------------------------------------------------------

set @imageurl = null
set @routeurl = N'zasob-lista'
set @title = N'Zasoby aplikacji'
set @description = null

INSERT INTO [dbo].[AdmMenuItem]
([ParentId],[ImageUrl],[RouteUrl],[Title],[Description],[DateCreated],[DateModified],[IsActive],[IsRemoved],[RowGuid],[AuthorId])
VALUES
(@parentid,@imageurl,@routeurl,@title,@description,GETDATE(),NULL,1,0,NEWID(),NEWID())


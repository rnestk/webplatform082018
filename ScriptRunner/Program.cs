﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPlatform.CORE;

namespace ScriptRunner
{
    class Program
    {
        private static string _connectionString;
        private static string _scriptsPath;

        static int Execute(FileInfo fInfo)
        {
            string script = File.ReadAllText(fInfo.FullName, Encoding.Default);
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    connection.Open();
                    command.Connection = connection;
                    command.CommandText = script;
                    return command.ExecuteNonQuery();
                }
            }         
        }

        static void Main(string[] args)
        {
            Console.WriteLine("START");
            _connectionString = @"Data Source=DESKTOP-CGD1C99\SQLEXPRESS;Initial Catalog=WebPlatformDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            _scriptsPath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, @"...\Scripts");

            DirectoryInfo dInfo = new DirectoryInfo(_scriptsPath);
            FileInfo[] sqlFiles = dInfo.GetFiles("*.sql");
            
            if(sqlFiles.Count() > 0)
            {
                foreach (FileInfo finfo in sqlFiles)
                {
                    Execute(finfo);
                }
            }


            Console.WriteLine("END");
        }
    }
}

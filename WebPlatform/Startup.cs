﻿using Microsoft.Owin;
using Owin;
using System.Data.Entity;
using WebPlatform.Models;

[assembly: OwinStartupAttribute(typeof(WebPlatform.Startup))]
namespace WebPlatform
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            Database.SetInitializer<ApplicationDbContext>(null);
        }
    }
}

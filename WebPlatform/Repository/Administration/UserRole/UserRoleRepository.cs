﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebPlatform.CORE;
using WebPlatform.Models.Administration;

namespace WebPlatform.Repository.Administration
{
    public class UserRoleRepository : BaseRepository<UserRole>
    {
        public UserRoleRepository(DbContext ctx)
        {
            _db = ctx;
        }
    }
}
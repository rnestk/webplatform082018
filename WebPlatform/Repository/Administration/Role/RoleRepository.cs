﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebPlatform.CORE;
using WebPlatform.Models.Administration;

namespace WebPlatform.Dao.Administration
{
    public class RoleRepository : BaseRepository<Role>
    {
        public RoleRepository(DbContext ctx)
        {
            _db = ctx;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebPlatform.CORE;
using WebPlatform.Models.Administration;

namespace WebPlatform.Repository.Administration
{
    public class RoleResourceRepository : BaseRepository<RoleResource>
    {
        public RoleResourceRepository(DbContext ctx)
        {
            _db = ctx;
        }
    }
}
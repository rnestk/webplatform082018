﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebPlatform.CORE;
using WebPlatform.Models.Administration;

namespace WebPlatform.Dao.Administration
{
    public class MenuItemRepository : BaseRepository<MenuItem>
    {
        public MenuItemRepository(DbContext ctx)
        {
            _db = ctx;
        }
    }
}
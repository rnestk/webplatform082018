﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using WebPlatform.CORE;
using WebPlatform.Models;

namespace WebPlatform.Dao.Administration
{
    public class UserRepository
    {
        protected DbContext _db;

        public UserRepository(DbContext ctx)
        {
            _db = ctx;
        }

        public event Action<ApplicationUser> BeforeAdd;
        public event Action<ApplicationUser, int> AfterAdd;
        public event Action<ApplicationUser, ApplicationUser> BeforeUpdate;
        public event Action<ApplicationUser, int> AfterUpdate;
        public event Action<ApplicationUser, bool> BeforeDelete;
        public event Action<ApplicationUser, int, bool> AfterDelete;
        public event Action<ApplicationUser, ApplicationUser> BeforeAddOrUpdate;
        public event Action<ApplicationUser, int> AfterAddOrUpdate;

        protected  IQueryable<ApplicationUser> FilteredList()
        {
            return _db.Set<ApplicationUser>().Where(e => !e.IsRemoved && e.IsActive);
        }

        public  ApplicationUser GetById(string id)
        {
            return _db.Set<ApplicationUser>().FirstOrDefault(e => e.RowGuid == id);
        }

        

        public async Task<ApplicationUser> GetByIdAsync(string id)
        {
            return await Task.Run(() =>
            {
                return _db.Set<ApplicationUser>().FirstOrDefault(e => e.RowGuid == id);
            });
        }


        public  IQueryable<ApplicationUser> GetAll()
        {
            return FilteredList();
        }

        public  IQueryable<ApplicationUser> GetAllWithNonFiltered()
        {
            return _db.Set<ApplicationUser>();
        }

        public  async Task<IQueryable<ApplicationUser>> GetAllAsync()
        {
            return await Task.Run(() =>
            {
                return FilteredList();
            });
        }

        public  async Task<IQueryable<ApplicationUser>> GetAllWithNonFilteredAsync()
        {
            return await Task.Run(() =>
            {
                return _db.Set<ApplicationUser>();
            });
        }

        public  int Add(ApplicationUser entity)
        {
            int result = 0;
            entity.DateCreated = DateTime.Now;
            entity.RowGuid = Guid.NewGuid().ToString();
            entity.IsActive = true;
            BeforeAdd?.Invoke(entity);
            ApplicationUser added = _db.Set<ApplicationUser>().Add(entity);
            try
            {
                result = _db.SaveChanges();
            }
            catch (DbEntityValidationException dexc)
            {
                throw;
            }

            AfterAdd?.Invoke(added, result);
            return result;
        }

        public async  Task<int> AddAsync(ApplicationUser entity)
        {
            return await Task.Run(() =>
            {
                entity.DateCreated = DateTime.Now;
                entity.RowGuid = Guid.NewGuid().ToString();
                entity.IsActive = true;
                BeforeAdd?.Invoke(entity);
                ApplicationUser added = _db.Set<ApplicationUser>().Add(entity);
                int result = _db.SaveChanges();
                AfterAdd?.Invoke(added, result);
                return result;
            });
        }

        public int Update(ApplicationUser entity)
        {
            ApplicationUser original = _db.Set<ApplicationUser>().FirstOrDefault(e => e.Id == entity.Id);
            entity.DateModified = DateTime.Now;
            BeforeUpdate?.Invoke(original, entity);
            _db.Entry(original).CurrentValues.SetValues(entity);
            int result = _db.SaveChanges();
            AfterUpdate?.Invoke(entity, result);
            return result;
        }


        public  async Task<int> UpdateAsync(ApplicationUser entity)
        {
            return await Task.Run(() =>
            {
                ApplicationUser original = _db.Set<ApplicationUser>().FirstOrDefault(e => e.Id == entity.Id);
                entity.DateModified = DateTime.Now;
                BeforeUpdate?.Invoke(original, entity);
                _db.Entry(original).CurrentValues.SetValues(entity);
                int result = _db.SaveChanges();
                AfterUpdate?.Invoke(entity, result);
                return result;
            });
        }

        public  int AddOrUpdate(ApplicationUser entity)
        {
            ApplicationUser original = _db.Set<ApplicationUser>().FirstOrDefault(e => e.Id == entity.Id);
            int result = 0;
            if (null == original)
            {
                entity.DateCreated = DateTime.Now;
                entity.RowGuid = Guid.NewGuid().ToString();
                entity.IsActive = true;
                BeforeAddOrUpdate?.Invoke(original, entity);
                _db.Set<ApplicationUser>().Add(entity);
                result = _db.SaveChanges();
                AfterAddOrUpdate?.Invoke(entity, result);
            }
            else
            {
                entity.DateModified = DateTime.Now;
                BeforeAddOrUpdate?.Invoke(original, entity);
                _db.Entry(original).CurrentValues.SetValues(entity);
                result = _db.SaveChanges();
                AfterAddOrUpdate?.Invoke(entity, result);
            }
            return result;
        }

        public  async Task<int> AddOrUpdateAsync(ApplicationUser entity)
        {
            return await Task.Run(() =>
            {
                ApplicationUser original = _db.Set<ApplicationUser>().FirstOrDefault(e => e.Id == entity.Id);
                int result = 0;
                if (null == original)
                {
                    entity.DateCreated = DateTime.Now;
                    entity.RowGuid = Guid.NewGuid().ToString();
                    entity.IsActive = true;
                    BeforeAddOrUpdate?.Invoke(original, entity);
                    _db.Set<ApplicationUser>().Add(entity);
                    result = _db.SaveChanges();
                    AfterAddOrUpdate?.Invoke(entity, result);
                }
                else
                {
                    entity.DateModified = DateTime.Now;
                    BeforeAddOrUpdate?.Invoke(original, entity);
                    _db.Entry(original).CurrentValues.SetValues(entity);
                    result = _db.SaveChanges();
                    AfterAddOrUpdate?.Invoke(entity, result);

                }
                return result;
            });
        }

        public  int Deactivate(ApplicationUser entity)
        {
            ApplicationUser original = _db.Set<ApplicationUser>().FirstOrDefault(e => e.Id == entity.Id);
            entity.DateModified = DateTime.Now;
            entity.IsActive = false;
            _db.Entry(original).CurrentValues.SetValues(entity);
            return _db.SaveChanges();
        }

        public  async Task<int> DeactivateAsync(ApplicationUser entity)
        {
            return await Task.Run(() =>
            {
                ApplicationUser original = _db.Set<ApplicationUser>().FirstOrDefault(e => e.Id == entity.Id);
                entity.DateModified = DateTime.Now;
                entity.IsActive = false;
                _db.Entry(original).CurrentValues.SetValues(entity);
                return _db.SaveChanges();
            });
        }

        public int Activate(ApplicationUser entity)
        {
            ApplicationUser original = _db.Set<ApplicationUser>().FirstOrDefault(e => e.Id == entity.Id);
            entity.DateModified = DateTime.Now;
            entity.IsActive = true;
            _db.Entry(original).CurrentValues.SetValues(entity);
            return _db.SaveChanges();
        }

        public async Task<int> ActivateAsync(ApplicationUser entity)
        {
            return await Task.Run(() =>
            {
                ApplicationUser original = _db.Set<ApplicationUser>().FirstOrDefault(e => e.Id == entity.Id);
                entity.DateModified = DateTime.Now;
                entity.IsActive = true;
                _db.Entry(original).CurrentValues.SetValues(entity);
                return _db.SaveChanges();
            });
        }

        public  int Delete(ApplicationUser entity)
        {
            ApplicationUser original = _db.Set<ApplicationUser>().FirstOrDefault(e => e.Id == entity.Id);
            entity.DateModified = DateTime.Now;
            entity.IsActive = false;
            entity.IsRemoved = true;
            BeforeDelete?.Invoke(entity, false);
            _db.Entry(original).CurrentValues.SetValues(entity);
            int result = _db.SaveChanges();
            AfterDelete?.Invoke(entity, result, false);
            return result;
        }

        public  async Task<int> DeleteAsync(ApplicationUser entity)
        {
            return await Task.Run(() =>
            {
                ApplicationUser original = _db.Set<ApplicationUser>().FirstOrDefault(e => e.Id == entity.Id);
                entity.DateModified = DateTime.Now;
                entity.IsActive = false;
                entity.IsRemoved = true;
                BeforeDelete?.Invoke(entity, false);
                _db.Entry(original).CurrentValues.SetValues(entity);
                int result = _db.SaveChanges();
                AfterDelete?.Invoke(entity, result, false);
                return result;
            });
        }

        public  int DeletePernament(ApplicationUser entity)
        {
            BeforeDelete?.Invoke(entity, true);
            _db.Set<ApplicationUser>().Remove(entity);
            int result = _db.SaveChanges();
            AfterDelete?.Invoke(entity, result, true);
            return result;
        }

        public  async Task<int> DeletePernamentAsync(ApplicationUser entity)
        {
            return await Task.Run(() =>
            {
                BeforeDelete?.Invoke(entity, true);
                _db.Set<ApplicationUser>().Remove(entity);
                int result = _db.SaveChanges();
                AfterDelete?.Invoke(entity, result, true);
                return result;
            });
        }

        public  ApplicationUser ModelToModel(ApplicationUser dest, ApplicationUser source)
        {
            Type sourceType = source.GetType();
            Type destType = dest.GetType();

            PropertyInfo[] destinfo = destType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty);
            PropertyInfo[] sourceInfo = sourceType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty);

            for (int i = 0; i < destinfo.Length; i++)
            {
                if (destinfo[i].CanWrite && destinfo[i].GetSetMethod(true) != null)
                {
                    string name = destinfo[i].Name;
                    var property = sourceInfo.Where(s => s.Name == name).FirstOrDefault();
                    if (null != property)
                    {
                        var value = property.GetValue(source, null);
                        if (sourceInfo[i] != null && null != value)
                        {
                            destinfo[i].SetValue(dest, Convert.ChangeType(value, destinfo[i].GetValue(dest, null).GetType(), null));
                        }
                    }


                }
            }

            return dest;
        }
    }
}
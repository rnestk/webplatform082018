﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPlatform.CORE.Helpers
{
    public class MapperHelper
    {

        public static TEntity ToEntity<TEntity, TViewModel>(TViewModel model) where TEntity : new() where TViewModel : new()
        {
            return (TEntity)Mapper.Map(model, typeof(TViewModel), typeof(TEntity));
        }

        public static TViewModel ToViewModel<TEntity, TViewModel>(TEntity entity) where TEntity : new() where TViewModel : new()
        {
            return (TViewModel)Mapper.Map(entity, typeof(TEntity), typeof(TViewModel));
        }

        public static TViewModel ToModel<TEntity, TViewModel>(TEntity entity) where TEntity : new() where TViewModel : new()
        {
            return (TViewModel)Mapper.Map(entity, typeof(TEntity), typeof(TViewModel));
        }

        public static IList<TEntity> ToEntitiesList<TEntity, TViewModel>(IEnumerable<TViewModel> list) where TEntity : new() where TViewModel : new()
        {
            var entities = new List<TEntity>();

            foreach (var item in list)
            {
                TEntity entity = ToEntity<TEntity, TViewModel>(item);
                entities.Add(entity);
            }

            return entities;
        }

        public static IList<TModel> ToModelList<TEntity, TModel>(IEnumerable<TEntity> list) where TEntity : new() where TModel : new()
        {
            var modelList = new List<TModel>();

            foreach (var item in list)
            {
                TModel model = ToViewModel<TEntity, TModel>(item);
                modelList.Add(model);
            }

            return modelList;
        }

        public static IList<TModel> ToModelList<TEntity, TModel>(IQueryable<TEntity> list) where TEntity : new() where TModel : new()
        {
            var modelList = new List<TModel>();

            foreach (var item in list)
            {
                TModel model = ToViewModel<TEntity, TModel>(item);
                modelList.Add(model);
            }

            return modelList;
        }

    }
}
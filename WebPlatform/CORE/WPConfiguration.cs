﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace WebPlatform.CORE
{
    public class WPConfiguration
    {
        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["WebPlatform"].ConnectionString ; }
           
        }
        public static string CurrentUserRowGuid { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace WebPlatform.CORE
{
    public static class Extension
    {
        public static IEnumerable<TSource> OrderBy<TSource>(this IEnumerable<TSource> source, string propertyName) where TSource : new()
        {
            if (string.IsNullOrEmpty(propertyName)) return source;
            Type type = typeof(TSource);
            Func<TSource, object> lambda = (s) =>
            {
                if (null == s) return new object();
                return
                Convert.ChangeType(type.GetProperty(propertyName)?.GetValue(s, null),
                type.GetProperty(propertyName)?.GetValue(s, null) == null ? new object().GetType() : type.GetProperty(propertyName)?.GetValue(s, null).GetType());
            };
            return source.OrderBy(lambda).ToList();
        }

        public static IEnumerable<TSource> OrderByDescending<TSource>(this IEnumerable<TSource> source, string propertyName) where TSource : new()
        {
            if (string.IsNullOrEmpty(propertyName)) return source;
            Type type = typeof(TSource);

            Func<TSource, object> lambda = (s) =>
            {
                if (null == s) return new object();
                return
                Convert.ChangeType(type.GetProperty(propertyName)?.GetValue(s, null),
                type.GetProperty(propertyName)?.GetValue(s, null) == null ? new object().GetType() : type.GetProperty(propertyName)?.GetValue(s, null).GetType());
            };
            return source.OrderByDescending(lambda).ToList();
        }

        public static IEnumerable<TSource> OrderBy<TSource>(this IEnumerable<TSource> source, string propertyName, string sortOrder) where TSource : new()
        {
            if (string.IsNullOrEmpty(propertyName)) return source;
            if (string.IsNullOrEmpty(sortOrder)) return source;
            Type type = typeof(TSource);

            Func<TSource, object> lambda = (s) =>
            {
                if (null == s) return new object();
                return
                Convert.ChangeType(type.GetProperty(propertyName)?.GetValue(s, null),
                type.GetProperty(propertyName)?.GetValue(s, null) == null ? new object().GetType() : type.GetProperty(propertyName)?.GetValue(s, null).GetType());
            };
            if(sortOrder.ToLower() == "desc")
            {
                return source.OrderByDescending(lambda).ToList();
            }
            return source.OrderBy(lambda).ToList();
        }

        public static IOrderedEnumerable<TSource> OrderBy<TSource>(this IQueryable<TSource> source, string propertyName, string sortOrder) where TSource : new()
        {
            //if (string.IsNullOrEmpty(propertyName)) return source;
            //if (string.IsNullOrEmpty(sortOrder)) return source;
            Type type = typeof(TSource);

            Func<TSource, object> lambda = (s) =>
            {
                if (null == s) return new object();
                return
                Convert.ChangeType(type.GetProperty(propertyName)?.GetValue(s, null),
                type.GetProperty(propertyName)?.GetValue(s, null) == null ? new object().GetType() : type.GetProperty(propertyName)?.GetValue(s, null).GetType());
            };
            if (sortOrder.ToLower() == "desc")
            {
                return source.OrderByDescending(lambda);
            }
            return source.OrderBy(lambda);
        }

        public static IEnumerable<TSource> Where<TSource>(this IEnumerable<TSource> source, string search, params string[] properties)
        {
            if (string.IsNullOrEmpty(search)) return source;
            List<TSource> list = new List<TSource>();
            Type type = typeof(TSource);
            if(properties.Any())
            {
                foreach (var property in properties)
                {
                    try
                    {
                        Func<TSource, bool> predicate = (s) => { return (type.GetProperty(property).GetValue(s, null)?.ToString().ToLower()).Contains(search.ToLower()); };
                        if (null == predicate) continue;
                        var resultList = source.Where(predicate);
                        if (null != resultList && resultList.Any())
                        {
                            list.AddRange(resultList);
                        }
                    }
                    catch
                    {
                        continue;
                    }
                    
                }
            }
            
            return list;
        }

        public static IEnumerable<TSource> Where<TSource>(this IQueryable<TSource> source, string search, params string[] properties)
        {
            if (string.IsNullOrEmpty(search))
            {
                foreach (var item in source)
                {
                    yield return item;
                }
                
            }
            else
            {

                Type type = typeof(TSource);
                if (properties.Any())
                {
                    foreach (var property in properties)
                    {
                        Func<TSource, bool> predicate = (s) => { return (type.GetProperty(property).GetValue(s, null)?.ToString().ToLower()).Contains(search.ToLower()); };
                        var resultList = source.Where(predicate);
                        if (resultList.Any())
                        {
                            foreach (var element in resultList)
                            {
                                yield return element;
                            }
                            yield break;
                        }
                    }
                }

            }
            
            

        }


    }
}
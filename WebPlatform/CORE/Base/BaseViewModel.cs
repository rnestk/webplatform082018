﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebPlatform.CORE.Helpers;
using WebPlatform.Models.Administration;

namespace WebPlatform.CORE
{
    public abstract class BaseViewModel<TModel> where TModel : BaseModel, new ()
    {
        protected IList<TModel> _elements;

        public IList<TModel> Elements
        {
            get { return _elements; }
            set {
                _elements = value;
            }
        }

        

        public BaseViewModel()
        {
            _elements = new List<TModel>();
        }

        public TModel this[int index]
        {
            get { return _elements[index]; }
        }

        [ScaffoldColumn(false)]
        public int ElementsTotalCount { get; set; }
        [ScaffoldColumn(false)]
        public int Page { get; set; }
        [ScaffoldColumn(false)]
        public int PageSize { get; set; }
        [ScaffoldColumn(false)]
        public string PropertyName { get; set; }
        [ScaffoldColumn(false)]
        public string Search { get; set; }
        [ScaffoldColumn(false)]
        public string SortOrder { get; set; }
        [ScaffoldColumn(false)]
        public int Skip { get; set; }

        [Key]
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Display(Name = "Data utworzenia")]
        public DateTime DateCreated { get; set; }

        [Display(Name = "Data modyfikacji")]
        public DateTime? DateModified { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Czy aktywne")]
        public bool IsActive { get; set; }

        [ScaffoldColumn(false)]
        public bool IsRemoved { get; set; }

        [ScaffoldColumn(false)]
        public string RowGuid { get; set; }

        [ScaffoldColumn(false)]
        public string AuthorId { get; set; }

       

        public void HandleCollection<TEntity>(IQueryable<TEntity> entities, int pageSize, params string[] properties) where TEntity : BaseEntity, new()
        {
            var parameters = HttpContext.Current.Request.Params;
            bool parsed = false;
            PageSize = pageSize;
            PropertyName = parameters["PropertyName"];
            SortOrder = parameters["SortOrder"];
            Search = parameters["Search"];

            if (string.IsNullOrEmpty(parameters["Page"]))
            {
                Page = 1;
            }
            else
            {
                int ipage = 1;
                parsed = int.TryParse(parameters["Page"], out ipage);
                Page = ipage;
            }

            entities = entities.Where(Search, properties).AsQueryable();
            ElementsTotalCount = entities.Count();
            Skip = (Page * PageSize) - PageSize;
            var mapped = MapperHelper.ToModelList<TEntity, TModel>(entities);
            Elements = mapped
                .OrderBy(PropertyName, SortOrder)
                .Skip(Skip)
                .Take(PageSize).ToList();

        }

        public void HandleCollection(IList<TModel> entities, int pageSize, params string[] properties)
        {
            var parameters = HttpContext.Current.Request.Params;
            bool parsed = false;
            PageSize = pageSize;
            PropertyName = parameters["PropertyName"];
            SortOrder = parameters["SortOrder"];
            Search = parameters["Search"];

            if (string.IsNullOrEmpty(parameters["Page"]))
            {
                Page = 1;
            }
            else
            {
                int ipage = 1;
                parsed = int.TryParse(parameters["Page"], out ipage);
                Page = ipage;
            }

            entities = entities.Where(Search, properties).ToList();
            ElementsTotalCount = entities.Count();
            Skip = (Page * PageSize) - PageSize;
            Elements = entities
                .OrderBy(PropertyName, SortOrder)
                .Skip(Skip)
                .Take(PageSize).ToList();

        }


       

    }
}
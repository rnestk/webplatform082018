﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebPlatform.CORE
{
    public abstract class BaseModel
    {
        [Key]
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Display(Name = "Data utworzenia")]
        public DateTime DateCreated { get; set; }

        [Display(Name = "Data modyfikacji")]
        public DateTime? DateModified { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Czy aktywne")]
        public bool IsActive { get; set; }

        [ScaffoldColumn(false)]
        public bool IsRemoved { get; set; }

        [ScaffoldColumn(false)]
        public string RowGuid { get; set; }

        [ScaffoldColumn(false)]
        public string AuthorId { get; set; }

       
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPlatform.CORE.Helpers;

namespace WebPlatform.CORE
{
    public abstract class BaseService<TModel, TEntity>  where TEntity : BaseEntity, new() where TModel : BaseModel, new()
    {
        protected BaseRepository<TEntity> _dao;
        
        public void OnBeforeAdd(Action<TEntity> ac)
        {
            _dao.BeforeAdd += ac;
        }

        public void OnAfterAdd(Action<TEntity, int> ac)
        {
            _dao.AfterAdd += ac;
        }

       
        public virtual IList<TModel> GetAll()
        {
            return MapperHelper.ToModelList<TEntity, TModel>(_dao.GetAll());
        }

        public virtual async Task<IList<TModel>> GetAllAsync()
        {
            var entities = await _dao.GetAllAsync();
            return await Task.Run(() => {
                return MapperHelper.ToModelList<TEntity, TModel>(entities);
            });
            
        }

        public virtual IList<TModel> GetAllWithDeleted()
        {
            return MapperHelper.ToModelList<TEntity, TModel>(_dao.GetAllWithNonFiltered());
        }

        public virtual async Task<IList<TModel>> GetAllWithDeletedAsync()
        {
            var entities = await _dao.GetAllWithNonFilteredAsync();
            return await Task.Run(() => 
            {
                return MapperHelper.ToModelList<TEntity, TModel>(entities);
            });

        }

        

        public virtual int Add(TModel model)
        {
            return _dao.Add(MapperHelper.ToEntity<TEntity, TModel>(model));
        }

        public virtual int AddOrUpdate(TModel model)
        {
            var mapped = MapperHelper.ToEntity<TEntity, TModel>(model);
            var entity = GetById(mapped.Id);
            if(null == entity) return _dao.Add(mapped);
            return _dao.Update(mapped);

        }

        public virtual async Task<int> AddOrUpdateAsync(TModel model)
        {
            return await Task.Run(() =>
            {
                return AddOrUpdate(model);
            });
            

        }

        public virtual async Task<int> AddAsync(TModel model)
        {
            return await Task.Run(() => 
            {
                var entity = MapperHelper.ToEntity<TEntity, TModel>(model);
                return _dao.AddAsync(entity);
            });
        }

        public virtual TModel GetByGuid(string id)
        {
            var entity = _dao.GetById(id);
            return MapperHelper.ToModel<TEntity, TModel>(entity);
        }

        public virtual async Task<TModel> GetByGuidAsync(string id)
        {
            var entity = await _dao.GetByIdAsync(id);
            return MapperHelper.ToModel<TEntity, TModel>(entity);
        }

        public virtual TModel GetById(int id)
        {
            var entity = _dao.GetById(id);
            return MapperHelper.ToModel<TEntity, TModel>(entity);
        }

        public virtual async Task<TModel> GetByIdAsync(int id)
        {
            var entity = await _dao.GetByIdAsync(id);
            return MapperHelper.ToModel<TEntity, TModel>(entity);
        }

        public virtual async Task<TModel> GetByIdAsync(string id)
        {
            var entity = await _dao.GetByIdAsync(id);
            return await Task.Run(() => 
            {
                return MapperHelper.ToModel<TEntity, TModel>(entity);
            }); 
        }

        public virtual int Update(TModel model)
        {
            var entity = MapperHelper.ToEntity<TEntity, TModel>(model);
            return _dao.Update(entity);
        }

        public virtual async Task<int> UpdateAsync(TModel model)
        {
            var entity = MapperHelper.ToEntity<TEntity, TModel>(model);
            return await Task.Run(() =>
            {
                return _dao.UpdateAsync(entity);
            });
            
        }

        public virtual async Task<int> DeleteAsync(TModel model)
        {
            var entity = MapperHelper.ToEntity<TEntity, TModel>(model);
            return await Task.Run(() =>
            {
                return _dao.DeleteAsync(entity);
            });
        }

        public virtual int Deactivate(TModel model)
        {
            var entity = MapperHelper.ToEntity<TEntity, TModel>(model);
            return _dao.Deactivate(entity);
        }

        public virtual int Delete(string id)
        {
            var model = GetByGuid(id);
            var entity = MapperHelper.ToEntity<TEntity, TModel>(model);
            return _dao.Delete(entity);
        }

        public virtual int DeletePernament(int id)
        {
            var model = GetById(id);
            var entity = MapperHelper.ToEntity<TEntity, TModel>(model);
            return _dao.DeletePernament(entity);
        }

        public virtual async Task<int> DeletePernamentAsync(int id)
        {
            return await Task.Run(()=> 
            {
                var model = GetById(id);
                var entity = MapperHelper.ToEntity<TEntity, TModel>(model);
                return _dao.DeletePernament(entity);

            });
            
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using WebPlatform.CORE;

namespace WebPlatform.Models.Administration
{
    public class Role : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string DisplayName { get; set; }
        public virtual ICollection<Resource> Resources { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }

        public Role()
        {
            Resources = new HashSet<Resource>();
            Users = new HashSet<ApplicationUser>();
        }
    }

    public class RoleConfiguration : EntityTypeConfiguration<Role>
    {
        public RoleConfiguration()
        {
            ToTable("AdmRole");
            HasKey(e => e.Id);
            Property(e => e.Name).IsRequired();
            Property(e => e.DisplayName).IsRequired();
            Property(e => e.Description).IsOptional();
            //HasMany(e => e.Resources).WithMany(r => r.Roles).Map(jt =>
            //{
            //    jt.MapLeftKey("RoleId");
            //    jt.MapRightKey("ResourceId");
            //    jt.ToTable("AdmRoleResource");
                
            //});
            //HasMany(e => e.Users).WithMany(r => r.AdmRoles).Map(jt =>
            //{
            //    jt.MapLeftKey("RoleId");
            //    jt.MapRightKey("UserId");
            //    jt.ToTable("AdmUserRole");

            //});
        }
    }
}
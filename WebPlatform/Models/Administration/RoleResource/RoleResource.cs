﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using WebPlatform.CORE;

namespace WebPlatform.Models.Administration
{
    public class RoleResource : BaseEntity
    {
        public int RoleId { get; set; }
        public int ResourceId { get; set; }
    }

    public class RoleResourceConfiguration : EntityTypeConfiguration<RoleResource>
    {
        public RoleResourceConfiguration()
        {
            ToTable("AdmRoleResource");
            HasKey(e => new { e.RoleId, e.ResourceId });
            //HasRequired(e => e.Roles).WithMany().HasForeignKey(e => e.RoleId);
            //HasRequired(e => e.Resources).WithMany().HasForeignKey(e => e.ResourceId);
            Ignore(e => e.Id);

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using WebPlatform.CORE;

namespace WebPlatform.Models.Administration
{
    public class Resource : BaseEntity
    {
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
        public string Symbol { get; set; }
        public virtual ICollection<Role> Roles { get; set; }

        public Resource()
        {
            Roles = new HashSet<Role>();
        }
    }


    public class ResourceConfiguration : EntityTypeConfiguration<Resource>
    {
        public ResourceConfiguration()
        {
            ToTable("AdmResource");
            HasKey(e => e.Id);
            Property(e => e.Action).IsUnicode().IsRequired();
            Property(e => e.Controller).IsUnicode().IsRequired();
            Property(e => e.Description).IsUnicode().IsRequired();
            Property(e => e.Symbol).IsUnicode().IsRequired();
        }
    }
}
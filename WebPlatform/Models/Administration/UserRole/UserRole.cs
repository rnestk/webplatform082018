﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using WebPlatform.CORE;

namespace WebPlatform.Models.Administration
{
    public class UserRole : BaseEntity
    {
        public int RoleId { get; set; }
        public string UserId { get; set; }
    }

    public class UserRoleConfiguration : EntityTypeConfiguration<UserRole>
    {
        public UserRoleConfiguration()
        {
            ToTable("AdmUserRole");
            HasKey(e => new { e.RoleId, e.UserId });           
            Ignore(e => e.Id);

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using WebPlatform.CORE;

namespace WebPlatform.Models.Administration
{
    public class MenuItem : BaseEntity
    {
        public int? ParentId { get; set; }
        public string ImageUrl { get; set; }
        public string RouteUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }

    public class MenuItemConfiguration : EntityTypeConfiguration<MenuItem>
    {
        public MenuItemConfiguration()
        {
            ToTable("AdmMenuItem");
            HasKey(e => e.Id);
            Property(e => e.Title).IsRequired();
            Property(e => e.Description).IsOptional();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using WebPlatform.CORE;
using WebPlatform.Models.Administration;

namespace WebPlatform.Models
{
    
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base(WPConfiguration.ConnectionString, throwIfV1Schema: false)
        {
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ApplicationDbContext>());
            Database.SetInitializer<ApplicationDbContext>(null);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); // This needs to go before the other rules!

            modelBuilder.Entity<ApplicationUser>().ToTable("AdmUser");
            modelBuilder.Entity<IdentityRole>().ToTable("IdentityRole");
            modelBuilder.Entity<IdentityUserRole>().ToTable("IdentityUserRole");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("IdentityUserClaim");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("IdentityUserLogin");

            modelBuilder.Configurations.Add(new ResourceConfiguration());
            modelBuilder.Configurations.Add(new MenuItemConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Configurations.Add(new RoleResourceConfiguration());
            modelBuilder.Configurations.Add(new UserRoleConfiguration());
        }

        public virtual DbSet<Resource> ResourceSet { get; set; }
        public virtual DbSet<MenuItem> MenuItemSet { get; set; }
        public virtual DbSet<Role> RoleSet { get; set; }
        public virtual DbSet<RoleResource> RoleResourcesSet { get; set; }
        public virtual DbSet<UserRole> UserRoleSet { get; set; }
    }
}
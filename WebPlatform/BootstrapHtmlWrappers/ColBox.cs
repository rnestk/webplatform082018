﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPlatform
{
    public class ColBox : Box
    {
        public ColBox(uint size = 0, uint offset = 0)
        {
            _builder = new System.Web.Mvc.TagBuilder("div");
            if(offset > 11)
            {
                offset = 0;
            }
            if(size > 0 && size <= 12)
            {
                _class += string.Format("col-{0} {1} ", size, offset > 0 ? "offset-" + offset.ToString() : "");
            }
            else
            {
                _class += "col ";
            }
            
        }
    }
}
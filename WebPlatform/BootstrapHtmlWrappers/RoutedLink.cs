﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebPlatform
{
    public class RoutedLink : Box
    {
        public string _linkText { get; protected set; }
        public string _routeUrl { get; protected set; }
        

        public RoutedLink(string routeUrl)
        {
            _builder = new System.Web.Mvc.TagBuilder("a");
            _routeUrl = routeUrl;
            _class += "applink btn btn-default ";
        }

        public override MvcHtmlString GetStartTag()
        {
            base.GetStartTag();
            if (!string.IsNullOrEmpty(_routeUrl)) _builder.MergeAttribute("href", _routeUrl);
            return MvcHtmlString.Create(_builder.ToString(TagRenderMode.StartTag));
        }

    }
}
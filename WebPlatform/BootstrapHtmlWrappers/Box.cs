﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebPlatform
{
    public class Box : IBox
    {
        protected TagBuilder _builder;
        protected string _id;
        protected string _class;
        protected string _style;
        protected string _accesskey;
        protected string _contentEditable;
        protected string _dir;
        protected string _draggable;
        protected string _hidden;
        protected string _lang;
        protected string _spellcheck;
        protected string _tabindex;
        protected string _title;
        protected IDictionary<string, string> _data;
        public Box()
        {
            _builder = new TagBuilder("div");
            _data = new Dictionary<string, string>();
        }

        public IBox Id(string id)
        {
            _id = id;
            return this;
        }

        public virtual MvcHtmlString GetStartTag()
        {
            if(!string.IsNullOrEmpty(_id)) _builder.MergeAttribute("id", _id);
            if (!string.IsNullOrEmpty(_style)) _builder.MergeAttribute("style", _style);
            if (!string.IsNullOrEmpty(_class)) _builder.AddCssClass(_class.Trim());
            if (!string.IsNullOrEmpty(_accesskey)) _builder.MergeAttribute("accesskey", _accesskey);
            if (!string.IsNullOrEmpty(_tabindex)) _builder.MergeAttribute("tabindex", _tabindex);
            if (!string.IsNullOrEmpty(_contentEditable)) _builder.MergeAttribute("contentEditable", _contentEditable);
            if (!string.IsNullOrEmpty(_dir)) _builder.MergeAttribute("dir", _dir);
            if (!string.IsNullOrEmpty(_draggable)) _builder.MergeAttribute("draggable", _draggable);
            if (!string.IsNullOrEmpty(_hidden)) _builder.MergeAttribute("hidden", _hidden);
            if (!string.IsNullOrEmpty(_lang)) _builder.MergeAttribute("lang", _lang);
            if (!string.IsNullOrEmpty(_spellcheck)) _builder.MergeAttribute("spellcheck", _spellcheck);
            if (!string.IsNullOrEmpty(_title)) _builder.MergeAttribute("title", _title);

            if(_data.Any())
            {
                foreach (var item in _data)
                {
                    var key = item.Key.Replace("data-", "");
                    _builder.MergeAttribute(string.Format("data-{0}", key), item.Value);
                }
            }

            return MvcHtmlString.Create(_builder.ToString(TagRenderMode.StartTag));
        }

        public virtual MvcHtmlString GetEndTag()
        {
            return MvcHtmlString.Create(_builder.ToString(TagRenderMode.EndTag));
        }

        public void Dispose()
        {
            
        }

        public virtual IBox AddClass(string cls)
        {
            _class += cls + " ";
            return this;
        }

        public virtual IBox AddData(string key, string value)
        {
            if(!_data.ContainsKey(key))
            {
                _data.Add(key, value);
            }
            return this;
        }

        public virtual IBox Style(string style)
        {
            _style = style;
            return this;
        }

        public virtual IBox Accesskey(string value)
        {
            _accesskey = value;
            return this;
        }

        public virtual IBox ContentEditable(bool value)
        {
            if(value)
            {
                _contentEditable = "true";
            }
            else
            {
                _contentEditable = "false";
            }
            return this;
        }

        public virtual IBox Dir(string value)
        {
            _dir = value;
            return this;
        }

        public virtual IBox Draggable(bool value)
        {
            if (value)
            {
                _draggable = "true";
            }
            else
            {
                _draggable = "false";
            }
            return this;
        }

        public virtual IBox Hidden(bool value)
        {
            if (value)
            {
                _hidden = "true";
            }
            else
            {
                _hidden = "false";
            }
            return this;
        }

        public virtual IBox Lang(string value)
        {
            _lang = value;
            return this;
        }

        public virtual IBox Spellcheck(bool value)
        {
            if (value)
            {
                _spellcheck = "true";
            }
            else
            {
                _spellcheck = "false";
            }
            return this;
        }

        public virtual IBox Tabindex(string value)
        {
            _tabindex = value;
            return this;
        }

        public virtual IBox Title(string value)
        {
            _title = value;
            return this;
        }
    }
}
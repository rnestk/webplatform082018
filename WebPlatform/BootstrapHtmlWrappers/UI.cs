﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using WebPlatform.CORE;

namespace WebPlatform
{
    public static class InlineWrapper
    {

        public static UI UI(this HtmlHelper helper)
        {
            return new UI(helper);
        }

        
    }

    public class UI : IDisposable
    {
        protected TextWriter _writer;
        protected HtmlHelper _helper;
        protected IBox _box;
        public IBox Box { get { return _box; } }

        //public UI(TextWriter textWriter)
        //{
        //    _writer = textWriter;
        //}

        public UI(HtmlHelper helper)
        {
            _helper = helper;
            _writer = helper.ViewContext.Writer;
        }

        public UI Begin(IBox box)
        {
            _box = box;
            _writer.Write(box.GetStartTag());
            return this;
        }

        public UI Begin(RoutedLink box)
        {
            _box = box;
            _writer.Write(box.GetStartTag());
            return this;
        }

        public Table<TSource> StdTable<TSource>(BaseViewModel<TSource> model) where TSource : BaseModel, new()
        {
            var table = new StdTable<TSource>(model);
            var pagination = new StdPagination<TSource>(model);
            var caption = new StdCaption<TSource>();
            table.ApplyCaption(caption);
            table.ApplyPagination(pagination);
            return table;
        }

        public WPPopup Popup()
        {
            return new WPPopup();
        }

        public Autocomplete AutoComplete()
        {
            return new Autocomplete();
        }

        
        public void Dispose()
        {
            _writer.Write(_box.GetEndTag());
        }
    }




}
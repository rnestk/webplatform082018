﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebPlatform
{
    public class Autocomplete : Box, IHtmlString
    {
        protected TagBuilder _input;
        protected TagBuilder _fakeInput;
        protected string _url;
        protected string _selectWidth;
        protected TagBuilder _inputWrapper;

        public Autocomplete()
        {
            _input = new TagBuilder("input");
            _fakeInput = new TagBuilder("input");
            _inputWrapper = new TagBuilder("div");
        }

        public Autocomplete Url(string url)
        {
            _url = url;
            return this;
        }

        public Autocomplete SelectWidth(string width)
        {
            _selectWidth = width;
            return this;
        }

        public string ToHtmlString()
        {
            _fakeInput.MergeAttribute("data-autocomplete", _url);
            _fakeInput.MergeAttribute("data-width", _selectWidth);
            _input.MergeAttribute("id", _id);
            _input.MergeAttribute("name", _id);
            _input.MergeAttribute("hidden", "true");
            _fakeInput.MergeAttribute("id", _id + "_Fake");
            if (!string.IsNullOrEmpty(_class)) _fakeInput.AddCssClass(_class);
            _inputWrapper.InnerHtml += _fakeInput.ToString();
            _inputWrapper.InnerHtml += _input.ToString();
            return _inputWrapper.ToString();
        }
    }
}
﻿namespace WebPlatform
{
    public interface IPopupTrigger
    {
        IPopupTrigger Title(string title);
        IPopupTrigger Url(string url);
        string GetTitle();
    }
}
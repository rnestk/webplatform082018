﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebPlatform
{
    public class PopupTrigger : Box, IHtmlString, IPopupTrigger
    {
        private IWPPopup _popup;
        private string _actionUrl;

        public PopupTrigger(IWPPopup popup)
        {
            _popup = popup;
            _builder = new TagBuilder("button");
            _builder.AddCssClass("btn btn-default popup-trigger");
            _builder.MergeAttribute("type", "button");
            _builder.MergeAttribute("data-toggle", "modal");
            _builder.MergeAttribute("type", "button");
            _builder.MergeAttribute("data-target", popup.GetId());
        }

        public PopupTrigger(string popupid)
        {
            _popup = null;
            _builder = new TagBuilder("button");
            _builder.AddCssClass("btn btn-default popup-trigger");
            _builder.MergeAttribute("type", "button");
            _builder.MergeAttribute("data-toggle", "modal");
            _builder.MergeAttribute("type", "button");
            _builder.MergeAttribute("data-target", popupid);
            
        }

        public IPopupTrigger Url(string url)
        {
            _actionUrl = url;
            return this;
        }

        public string GetTitle()
        {
            return _title;
        }

        public new IPopupTrigger Title(string title)
        {
            _title = title;
            return this;
        }

        public string ToHtmlString()
        {
            _builder.MergeAttribute("data-url", string.Format("{0}", _actionUrl));
            _builder.InnerHtml += (_title + " ");
            return _builder.ToString();
        }

        public override string ToString()
        {
            return ToHtmlString();
        }
    }
}
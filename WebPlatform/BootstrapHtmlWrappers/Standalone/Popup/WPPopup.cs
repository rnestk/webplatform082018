﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebPlatform
{
    public class WPPopup : Box, IHtmlString, IWPPopup
    {
        private TagBuilder _modalDialog;
        private TagBuilder _modalContent;
        private TagBuilder _modalHeader;
        private TagBuilder _modalTitle;
        private TagBuilder _close;
        private TagBuilder _closeSpan;
        private TagBuilder _modalBody;
        private IPopupTrigger _trigger;
        private PopupSize _size;
        private bool _generateButton;

        public WPPopup()
        {
            InstantinateBuilders();
            AddAttributes();
            _size = PopupSize.Small;
        }


        public WPPopup Button(IPopupTrigger trigger)
        {
            _trigger = trigger;
            return this;
        }

        public WPPopup Button(string title)
        {
            
            _trigger = new PopupTrigger(this);
            _trigger.Title(title);
            _generateButton = true;
            return this;
        }

       

        public WPPopup Size(PopupSize size)
        {
            _size = size;
            return this;
        }

        public new WPPopup Title(string title)
        {
            _title = title;
            _modalTitle.InnerHtml += _title;
            return this;
        }

        public new WPPopup Id(string id)
        {
            _id = id;
            _builder.MergeAttribute("id", _id);
            return this;
        }

        private void AddAttributes()
        {
            _builder.AddCssClass("modal fade wppopup");
            _builder.MergeAttribute("tabindex", "-1");
            _builder.MergeAttribute("role", "dialog");
            _builder.MergeAttribute("aria-labelledby", "modal");
            _builder.MergeAttribute("aria-hidden", "true");

            _modalDialog.AddCssClass("modal-dialog");
            
            _modalDialog.MergeAttribute("role", "document");

            _modalContent.AddCssClass("modal-content");

            _modalHeader.AddCssClass("modal-header");

            _modalTitle.AddCssClass("modal-title");
            _modalTitle.InnerHtml += _title;

            _close.AddCssClass("close");
            _close.MergeAttribute("type", "button");
            _close.MergeAttribute("data-dismiss", "modal");
            _close.MergeAttribute("aria-label", "Close");

            _closeSpan.MergeAttribute("aria-hidden", "true");
            _closeSpan.InnerHtml += "&times;";

            _modalBody.AddCssClass("modal-body");
            _modalBody.MergeAttribute("data-container", "append");
           

        }

        private void InstantinateBuilders()
        {
            _builder = new TagBuilder("div");
            _modalDialog = new TagBuilder("div");
            _modalContent = new TagBuilder("div");
            _modalHeader = new TagBuilder("div");
            _modalTitle = new TagBuilder("h5");
            _close = new TagBuilder("button");
            _closeSpan = new TagBuilder("span");
            _modalBody = new TagBuilder("div");

        }

        public string ToHtmlString()
        {
            if (_size == PopupSize.Large) _modalDialog.AddCssClass("modal-lg");
            _close.InnerHtml += _closeSpan.ToString();
            _modalHeader.InnerHtml += _modalTitle.ToString();
            _modalHeader.InnerHtml += _close.ToString();
            _modalContent.InnerHtml += _modalHeader.ToString();
            _modalContent.InnerHtml += _modalBody.ToString();
            _modalDialog.InnerHtml += _modalContent.ToString();
            _builder.InnerHtml += _modalDialog.ToString();
            
            return ( _trigger == null ? _builder.ToString() : GenerateButton() + _builder.ToString());
        }

        private string GenerateButton()
        {
            if (!_generateButton) return "";
            TagBuilder buttonBuider = new TagBuilder("button");
            buttonBuider.MergeAttribute("type", "button");
            buttonBuider.MergeAttribute("data-toggle", "modal");
            buttonBuider.MergeAttribute("data-target", "#" + _id);
            buttonBuider.MergeAttribute("id", "button_" + _id);
            buttonBuider.AddCssClass("btn btn-default popup-trigger");
            buttonBuider.InnerHtml += _trigger.GetTitle();
            return buttonBuider.ToString();
        }

        public string GetId()
        {
            return _id;
        }
    }
}
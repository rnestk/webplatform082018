﻿using System.Web.Mvc;

namespace WebPlatform
{
    public interface IWPPopup
    {
        WPPopup Id(string id);
        string GetId();
        WPPopup Title(string title);
        string ToHtmlString();
    }
}
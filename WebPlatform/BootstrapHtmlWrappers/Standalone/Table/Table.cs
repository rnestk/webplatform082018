﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using WebPlatform.CORE;
using WebPlatform.CORE.Helpers;

namespace WebPlatform
{
    public abstract class Table<TSource> : IHtmlString where TSource : BaseModel, new()
    {
        protected BaseViewModel<TSource> _model;
        protected TagBuilder _tableBox;
        protected TagBuilder _tableBuilder;
        protected TagBuilder _headerBuilder;
        protected TagBuilder _bodyBuilder;
        protected TagBuilder _footerBuilder;
        protected TagBuilder _paginationBuilder;
        protected TagBuilder _captionBuilder;
        protected string _id;
        protected IList<string> _classList;
        protected IList<Column<TSource>> _columns;
        protected IList<Column<TSource>> _additionalColumns;
        protected IList<Column<TSource>> _forEachColumns;
        protected IList<object> _source;
        protected IList<List<string>> _rows;
        protected int _columnCount;
        protected int _rowIndex;
        protected bool _isPager;
        protected int _totalCount;
        protected Pagination<TSource> _pagination;
        protected Caption<TSource> _caption;
        protected string _title;
        protected bool _isSortable;
        protected bool _isSearchable;
        protected string[] _searchFields;
        protected IDictionary<int, string> _additionalData;

        public Table(BaseViewModel<TSource> model)
        {
            _model = model;
            _tableBox = new TagBuilder("div");
            _tableBuilder = new TagBuilder("table");
            _headerBuilder = new TagBuilder("thead");
            _bodyBuilder = new TagBuilder("tbody");
            _footerBuilder = new TagBuilder("tfoot");
            _captionBuilder = new TagBuilder("div");
            _columns = new List<Column<TSource>>();
            _additionalColumns = new List<Column<TSource>>();
            _rows = new List<List<string>>();
            _classList = new List<string>();
            _id = Guid.NewGuid().ToString();
            _columnCount = 0;
            _source = new List<object>();
            SetSource(model.Elements);
            _additionalData = new Dictionary<int, string>();
            _forEachColumns = new List<Column<TSource>>();
        }

        public virtual int RowIndex { get { return _rowIndex; } }

        public virtual Table<TSource> Title(string title)
        {
            _title = title;
            return this;
        }

        public virtual Table<TSource> Sortable()
        {
            _isSortable = true;
           
            return this;
        }

        public virtual Table<TSource> Searchable(params string[] fields)
        {
            _isSearchable = true;
            _searchFields = fields;
            return this;
        }

        
        public virtual Table<TSource> Id(string id)
        {
            _id = id;
            return this;
        }

        public virtual Table<TSource> AddClass(string cssClass)
        {
            _classList.Add(cssClass);
            return this;
        }

        public virtual Table<TSource> Data(string dataAttributeKey, string value)
        {
            string key = dataAttributeKey.StartsWith("data-") ? dataAttributeKey : "data-" + dataAttributeKey;
            _tableBuilder.MergeAttribute(key, value);
            return this;
        }


        public virtual Table<TSource> Column(string columnTitle, bool isSortable = false, string displayName = null, string format = null)
        {
            int maxid = _columns.Any() ? _columns.Max(c => c.Id) : 0;
            _columns.Add(new Column<TSource> { Title = columnTitle, Id = ++maxid, DisplayName = displayName ?? columnTitle, Format = format, IsSortable = isSortable });
            _columnCount++;
            return this;
        }

        public virtual Table<TSource> Column<T>(Expression<Func<TSource, T>> exp, bool isSortable = false, string displayName = null, string format = null)
        {
            int maxid = _columns.Any() ? _columns.Max(c => c.Id) : 0;
            string columnTitle = DynamicHelper.GetMemberName(exp);

            _columns.Add(new Column<TSource> { Title = columnTitle, Id = ++maxid, DisplayName = displayName ?? columnTitle, Format = format, IsSortable = isSortable });
            _columnCount++;
            return this;
        }

        public virtual Table<TSource> Column(Expression<Func<TSource, object>> exp, bool isSortable = false, string displayName = null, string format = null)
        {
            int maxid = _columns.Any() ? _columns.Max(c => c.Id) : 0;
            string columnTitle = DynamicHelper.GetMemberName(exp);
            _columns.Add(new Column<TSource> { Title = columnTitle, Id = ++maxid, DisplayName = displayName ?? columnTitle, Format = format, IsSortable = isSortable });
            _columnCount++;
            return this;
        }

       
        public virtual Table<TSource> Column(MvcHtmlString control, string columnTitle, bool isSortable = false, string displayName = null, string format = null)
        {
            int maxid = _columns.Any() ? _columns.Max(c => c.Id) : 0;
            _columns.Add(new Column<TSource> { Title = columnTitle, Id = ++maxid, DisplayName = displayName ?? columnTitle, Format = format, IsSortable = isSortable });
            _columnCount++;
            _additionalData.Add(new KeyValuePair<int, string>(maxid, control.ToString()));
            return this;
        }

        public virtual Table<TSource> ForEachColumn(string pattern, string columnTitle, bool isSortable = false, string displayName = null,
            params Expression<Func<TSource, object>>[] exp
            )
        {
            int maxid = _columns.Any() ? _columns.Max(c => c.Id) : 0;
            var column = new Column<TSource>
            {
                Title = columnTitle, Id = ++maxid, DisplayName = displayName ?? columnTitle, IsSortable = isSortable,
                Pattern = pattern, ColumnExpressions = exp
            };
            _columns.Add(column);
            _columnCount++;
            return this;
        }

        public virtual Table<TSource> Pagination()
        {
            _isPager = true;
            return this;
        }


        protected virtual void SetSource(IEnumerable<TSource> source)
        {
            var sourceList = source.Cast<object>().ToList();
            sourceList.ForEach((elem) =>
            {
                _source.Add(elem);
            });
        }

        protected virtual IEnumerable<TSource> GetSource()
        {
            return (IEnumerable<TSource>)_source;
        }

        protected virtual void AddColumns()
        {
            if (!_columns.Any()) return;
            TagBuilder tr = new TagBuilder("tr");

           

            foreach (var column in _columns)
            {
                
                TagBuilder td = new TagBuilder("td");
                td.MergeAttribute("id", "head_" + column.Id.ToString());
                td.MergeAttribute("data-title", column.Title?.ToString());

                TagBuilder p = new TagBuilder("p");
                p.InnerHtml += column.DisplayName;

                TagBuilder row = new TagBuilder("div");
                row.AddCssClass("row");
                TagBuilder colLeft = new TagBuilder("div");

                if (_isSortable && column.IsSortable)
                {
                    colLeft.AddCssClass("col-10");
                }
                else
                {
                    colLeft.AddCssClass("col-12");
                }

                colLeft.InnerHtml += p.ToString();

                if (_isSortable && column.IsSortable)
                {
                    TagBuilder colRight = new TagBuilder("div");
                    colRight.AddCssClass("col-2");

                    TagBuilder asc = new TagBuilder("a");
                    TagBuilder desc = new TagBuilder("a");

                    asc.MergeAttribute("href", "#");
                    desc.MergeAttribute("href", "#");

                    asc.MergeAttribute("title", "Sortowanie wstepujące");
                    desc.MergeAttribute("title", "Sortowanie zstepujące");

                    asc.MergeAttribute("data-col-title", column.Title?.ToString());
                    desc.MergeAttribute("data-col-title", column.Title?.ToString());

                    asc.MergeAttribute("data-column", _id);
                    desc.MergeAttribute("data-column", _id);

                    asc.MergeAttribute("data-property-id", _id);
                    asc.MergeAttribute("data-sort-asc-id", _id);
                    desc.MergeAttribute("data-property-id", _id);
                    desc.MergeAttribute("data-sort-desc-id", _id);
                    asc.InnerHtml += "<i class='far fa-caret-square-up'></i>";
                    desc.InnerHtml += "<i class='far fa-caret-square-down'></i>";
                    colRight.InnerHtml += asc.ToString();
                    colRight.InnerHtml += desc.ToString();

                    row.InnerHtml += colLeft.ToString();
                    row.InnerHtml += colRight.ToString();
                }
                else
                {
                    row.InnerHtml += colLeft.ToString();
                }
                td.InnerHtml = row.ToString();
                tr.InnerHtml += td.ToString();
            }
            _headerBuilder.InnerHtml += tr.ToString();
        }

        protected virtual void AddDataSource()
        {
            if (!_source.Any() || !_columns.Any()) return;
            foreach (var item in _source)
            {
                var cols = new List<string>();
                foreach (var column in _columns.OrderBy(c => c.Id))
                {
                    if(column.ColumnExpressions != null)
                    {
                        Type rowType = item.GetType();
                        object[] columns = new object[column.ColumnExpressions.Count()];
                        for (int i = 0; i < column.ColumnExpressions.Count(); i++)
                        {
                            string propertyName = DynamicHelper.GetMemberName(column.ColumnExpressions[i]);
                            var propertyRowValue = rowType.GetProperty(propertyName).GetValue(item, null);
                            columns[i] = propertyRowValue;
                        }

                        if (column.Id >= cols.Count - 1)
                        {
                            string value = string.Format(column.Pattern, columns);
                            cols.Add(value);
                        }
                        else
                        {
                            string value = string.Format(column.Pattern, columns);
                            cols.Insert(0, value);
                        }
                    }
                    else
                    {
                        var type = item.GetType();
                        var col = type.GetProperty(column.Title);
                        if (null != col)
                        {
                            var value = col.GetValue(item, null);
                            if (null != value)
                            {
                                if (!string.IsNullOrEmpty(column.Format))
                                {
                                    cols.Add(string.Format(column.Format, value));
                                }
                                else
                                {
                                    cols.Add(value.ToString());
                                }

                            }
                            else
                            {
                                cols.Add(string.Empty);
                            }
                        }
                    }
                    
                }

                if (_additionalData.Any())
                {
                    foreach (var add in _additionalData)
                    {
                        if (add.Key >= cols.Count - 1)
                        {
                            cols.Add(add.Value);
                        }
                        else
                        {
                            cols.Insert(add.Key, add.Value);
                        }

                    }
                }



                _rows.Add(cols);

            }

        }


        protected virtual void ProcessDataSource()
        {
            if (!_source.Any())
            {
                TagBuilder tr = new TagBuilder("tr");
                
                _rowIndex++;
                tr.MergeAttribute("id", _rowIndex.ToString());
                tr.MergeAttribute("data-table", _id);
                TagBuilder col = new TagBuilder("td");
                col.MergeAttribute("colspan", _columns.Count.ToString());
                col.InnerHtml = "Tabela nie zawiera danych";
                tr.InnerHtml += col.ToString();

                _bodyBuilder.InnerHtml += tr.ToString();
                return;
            }
            foreach (var cols in _rows)
            {
                TagBuilder tr = new TagBuilder("tr");
                _rowIndex++;
                tr.MergeAttribute("id", _rowIndex.ToString());
                tr.MergeAttribute("data-table", _id);
                foreach (var item in cols)
                {
                    TagBuilder col = new TagBuilder("td");
                    col.InnerHtml = item;
                    tr.InnerHtml += col.ToString();
                }

                _bodyBuilder.InnerHtml += tr.ToString();
            }
        }

        protected virtual void ProcessAttributes()
        {
            _tableBuilder.MergeAttribute("id", _id);
            if (_classList.Any())
            {
                foreach (var cl in _classList)
                {
                    _tableBuilder.AddCssClass(cl);
                }
            }
            else
            {
                _tableBuilder.AddCssClass("table table-bordered table-striped");
            }

        }

        public virtual void ApplyPagination(Pagination<TSource> pagination)
        {
            _pagination = pagination;
        }

        public virtual void ApplyCaption(Caption<TSource> caption)
        {
            _caption = caption;
        }

        protected virtual void HandleCaption()
        {
            if(null != _caption)
            {
                _caption.SetTitle(_title ?? "Tabela");
                _caption.SetId(_id);
                _caption.SetSource<TSource>(_model);
                _caption.IsSerchable = _isSearchable;
                _caption.SearchFields = _searchFields;
                _captionBuilder.InnerHtml = _caption.ToString();
            }
            
        }

        public virtual string ToHtmlString()
        {
            ProcessAttributes();
            AddColumns();
            AddDataSource();
            ProcessDataSource();
            HandleCaption();

            _tableBox.AddCssClass("table-outer-box");
            _tableBox.MergeAttribute("data-page", _model.Page.ToString());
            _tableBox.MergeAttribute("data-sort", _model.SortOrder);
            _tableBox.MergeAttribute("data-property", _model.PropertyName);
            _tableBox.MergeAttribute("data-search", _model.Search);

            _tableBuilder.InnerHtml += _captionBuilder.ToString();
            _tableBuilder.InnerHtml += _headerBuilder.ToString();
            _tableBuilder.InnerHtml += _bodyBuilder.ToString();

            if (null != _pagination && _isPager)
            {
                TagBuilder tr = new TagBuilder("tr");
                TagBuilder td = new TagBuilder("td");
                td.MergeAttribute("colspan", _columnCount.ToString());
                _pagination.SetId(_id);
                td.InnerHtml += _pagination.ToString();
                tr.InnerHtml += td.ToString();
                _footerBuilder.InnerHtml += tr.ToString();
            }

            _tableBuilder.InnerHtml += _footerBuilder.ToString();
            _tableBox.InnerHtml = _tableBuilder.ToString();

            return _tableBox.ToString();
        }


    }
}
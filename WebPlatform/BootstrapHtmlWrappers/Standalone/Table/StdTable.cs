﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPlatform.CORE;

namespace WebPlatform
{
    public class StdTable<TSource> : Table<TSource> where TSource : BaseModel, new()
    {
        public StdTable(BaseViewModel<TSource> model) : base(model)
        {
        }
    }
}
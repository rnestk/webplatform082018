﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebPlatform.CORE;

namespace WebPlatform
{
    public abstract class Pagination<TEntity> : IHtmlString where TEntity : BaseModel, new()
    {
        public TagBuilder _buttonsBuilder { get; set; }
        public TagBuilder _pagerBuilder;
        protected IList<Button> _buttons { get; set; }
        protected double _buttonsCount;
        protected bool _isVisible;
        protected string _tableId;
        protected BaseViewModel<TEntity> _model;

        public Pagination(BaseViewModel<TEntity> model)
        {
            _model = model;
            _buttonsBuilder = new TagBuilder("div");
            _pagerBuilder = new TagBuilder("div");
            _buttons = new List<Button>();
            _buttonsCount = CalculateButtonsCount();
        }

        protected virtual int CalculateButtonsCount()
        {
            int count = 0;
            if (_model.ElementsTotalCount > _model.PageSize)
            {
                return (int)Math.Ceiling((double)_model.ElementsTotalCount / (double)_model.PageSize);
            }
            return count;
        }

        protected virtual void CreateButtons()
        {
            int offset = 5;
            int current = _model.Page;
            int start = current - offset;
            int end = current + offset;

            if (end > (int)_buttonsCount) end = (int)_buttonsCount;

            if (start > 1)
            {
                CreateButtonHtml(1, "pager-add-button");
            }

            for (int i = start; i <= end; i++)
            {
                if (i > 0)
                    CreateButtonHtml(i);
            }


            if (_buttonsCount > end)
            {
                CreateButtonHtml((int)_buttonsCount, "pager-add-button");
            }
        }

        protected virtual void CreateButtonHtml(int i, string addClass = "")
        {
            Button a = new Button("a");
            a.Id = string.Format("button_{0}", i);
            a.IsActive = true;
            a.Number = i;
            a.CssClass = "btn btn-info pager-button " + addClass;
            a.MergeAttribute("data-table-id", _tableId);
            a.MergeAttribute("data-pager-id", _tableId);
            a.MergeAttribute("data-num-id", i.ToString());
            Route route = HttpContext.Current.Request.RequestContext.RouteData.Route as Route;
            string routeUrl = route.Url.Split('/')[0];
            //a.MergeAttribute("href", string.Format("/{0}/{1}/{2}/{3}/{4}", routeUrl, i, _model.PropertyName, _model.SortOrder, _model.Search));
            a.MergeAttribute("href", "#");

            if (_model.Page == i) a.CssClass += " current";
            _buttonsBuilder.InnerHtml += a.ToString();
        }

        public virtual string ToHtmlString()
        {
            CreateButtons();
            _pagerBuilder.InnerHtml += _buttonsBuilder.ToString();

            return _pagerBuilder.ToString();
        }

        public override string ToString()
        {
            return ToHtmlString();
        }

        internal void SetId(string id)
        {
            _tableId = id;
        }
    }
}
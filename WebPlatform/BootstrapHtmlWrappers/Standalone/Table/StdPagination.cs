﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPlatform.CORE;

namespace WebPlatform
{
    public class StdPagination<TEntity> : Pagination<TEntity> where TEntity : BaseModel, new()
    {
        public StdPagination(BaseViewModel<TEntity> model) : base(model)
        {
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToHtmlString()
        {
            return base.ToHtmlString();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        protected override int CalculateButtonsCount()
        {
            return base.CalculateButtonsCount();
        }

        protected override void CreateButtonHtml(int i, string addClass = "")
        {
            base.CreateButtonHtml(i, addClass);
        }

        protected override void CreateButtons()
        {
            base.CreateButtons();
        }
    }
}
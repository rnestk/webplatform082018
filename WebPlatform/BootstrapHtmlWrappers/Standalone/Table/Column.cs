﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using WebPlatform.CORE;

namespace WebPlatform
{
    

    public class Column<TSource> where TSource : BaseModel, new()
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string DisplayName { get; set; }
        public string Format { get; set; }
        public bool IsSortable { get; set; }
        public string Template { get; set; }
        public Expression<Func<TSource, object>>[] ColumnExpressions { get; set; }
        public string Pattern { get; set; }
    }
}
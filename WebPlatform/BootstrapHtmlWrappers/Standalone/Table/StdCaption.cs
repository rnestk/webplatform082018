﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPlatform.CORE;

namespace WebPlatform
{
    public class StdCaption<TEntity> : Caption<TEntity> where TEntity : BaseModel, new()
    {
        public StdCaption() : base()
        {
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToHtmlString()
        {
            return base.ToHtmlString();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        protected override string HandleSearchHtml()
        {
            return base.HandleSearchHtml();
        }
    }
}
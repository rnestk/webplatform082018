﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPlatform
{
    public class Container : Box
    {
        public Container(ContainerType type = ContainerType.Standard)
        {
            _builder = new System.Web.Mvc.TagBuilder("div");
            if(type == ContainerType.Standard)
            {
                _class += "container ";
            }
            else
            {
                _class += "container-fluid ";
            }
            
        }
    }
}
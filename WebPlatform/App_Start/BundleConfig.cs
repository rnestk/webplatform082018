﻿using System.Web;
using System.Web.Optimization;

namespace WebPlatform
{
    public class BundleConfig
    {
        // Aby uzyskać więcej informacji o grupowaniu, odwiedź stronę https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Użyj wersji deweloperskiej biblioteki Modernizr do nauki i opracowywania rozwiązań. Następnie, kiedy wszystko będzie
            // gotowe do produkcji, użyj narzędzia do kompilowania ze strony https://modernizr.com, aby wybrać wyłącznie potrzebne testy.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                      "~/Scripts/scrollbar/jquery.scrollbar.min.js",
                      "~/Scripts/jquery-ui-1.12.1.min.js",
                      "~/Scripts/slick/slick.js",
                      "~/Scripts/popup.js",
                      "~/Scripts/autocomplete.js",
                      "~/Scripts/main.js",
                      "~/Scripts/menu.js",
                      "~/Scripts/table.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                      "~/Scripts/jquery-ui-1.12.1.min.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.min.css",
                      "~/Scripts/scrollbar/jquery.scrollbar.css",
                      "~/Content/themes/base/all.css",
                      "~/Content/themes/base/menu.css",
                      "~/Scripts/slick/slick.css",
                      "~/Content/Hover/css/hover.css",
                      "~/Content/popup.css",
                      "~/Content/site.css",
                      "~/Content/themes/jquery-ui.autocomplete.theme.css"
                      ));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebPlatform
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // ROLES USERS

           // routes.MapRoute(
           //    name: "UserRoleChangeAssign",
           //    url: "change/{roleGuid}/{userGuid}",
           //    defaults:
           //    new
           //    {
           //        controller = "UserRole",
           //        action = "AssignChange"
           //    }
           //);

            routes.MapRoute(
                name: "UserRoleAssign",
                url: "role-uzytkownicy-dodawanie/{userGuid}",
                defaults:
                new
                {
                    controller = "UserRole",
                    action = "AssignRole"
                }
            );


            // ROLES RESOURCES

            //routes.MapRoute(
            //    name: "RoleChengeAssign",
            //    url: "change/{roleGuid}",
            //    defaults:
            //    new
            //    {
            //        controller = "RoleResource",
            //        action = "AssignChange"
            //    }
            //);

            routes.MapRoute(
                name: "RoleAssign",
                url: "role-zasoby-dodawanie/{resourceGuid}",
                defaults:
                new
                {
                    controller = "RoleResource",
                    action = "AssignRole"
                }
            );

            // ROLES

            routes.MapRoute(
                name: "RoleDelete",
                url: "rola-usuwanie/{rowGuid}",
                defaults:
                new
                {
                    controller = "Role",
                    action = "Delete"
                }
            );

            routes.MapRoute(
                name: "RoleEdit",
                url: "rola-edycja/{rowGuid}",
                defaults:
                new
                {
                    controller = "Role",
                    action = "Edit"
                }
            );

            routes.MapRoute(
                name: "RoleList",
                url: "role-lista",
                defaults:
                new
                {
                    controller = "Role",
                    action = "List"
                }
            );

            routes.MapRoute(
                name: "RoleAdd",
                url: "rola-nowa",
                defaults:
                new
                {
                    controller = "Role",
                    action = "Add"
                }
            );


            // USERS

            routes.MapRoute(
                name: "UserActivate",
                url: "uzytkownik-aktywacja/{rowGuid}",
                defaults:
                new
                {
                    controller = "User",
                    action = "Activate"
                }
            );

            routes.MapRoute(
                name: "UserDeactivate",
                url: "uzytkownik-zawieszanie/{rowGuid}",
                defaults:
                new
                {
                    controller = "User",
                    action = "Deactivate"
                }
            );

            routes.MapRoute(
                name: "UserDelete",
                url: "uzytkownik-usuwanie/{rowGuid}",
                defaults:
                new
                {
                    controller = "User",
                    action = "Delete"
                }
            );

            routes.MapRoute(
                name: "UserEdit",
                url: "uzytkownik-edycja/{rowGuid}",
                defaults:
                new
                {
                    controller = "User",
                    action = "Edit"
                }
            );

            routes.MapRoute(
                name: "UserActiveList",
                url: "uzytkownicy-nieaktywni-lista",
                defaults:
                new
                {
                    controller = "User",
                    action = "InActiveList"
                }
            );

            routes.MapRoute(
                name: "UserList",
                url: "uzytkownicy-aktywni-lista",
                defaults:
                new
                {
                    controller = "User",
                    action = "List"
                }
            );

            routes.MapRoute(
                name: "UserAdd",
                url: "uzytkownik-nowy",
                defaults:
                new
                {
                    controller = "User",
                    action = "Add"
                }
            );

            routes.MapRoute(
                name: "ResourceDelete",
                url: "zasob-usuwanie/{rowGuid}",
                defaults:
                new
                {
                    controller = "Resource",
                    action = "Delete"
                }
            );

            routes.MapRoute(
                name: "ResourceEdit",
                url: "zasob-edycja/{rowGuid}",
                defaults:
                new
                {
                    controller = "Resource",
                    action = "Edit"
                }
            );

            routes.MapRoute(
                name: "ResourceList",
                url: "zasob-lista",
                defaults:
                new
                {
                    controller = "Resource",
                    action = "List"
                }
            );

            routes.MapRoute(
                name: "ResourceAdd",
                url: "zasob-nowy",
                defaults:
                new
                {
                    controller = "Resource",
                    action = "Add"                    
                }
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Panel", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}

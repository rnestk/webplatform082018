﻿using AutoMapper;
using Ninject;
using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebPlatform.Models;
using WebPlatform.Models.Administration;
using WebPlatform.Services;
using WebPlatform.Services.Administration;
using WebPlatform.Services.Administration.User;
using WebPlatform.ViewModels.Administration;
using WebPlatform.ViewModels.Administration.User;

namespace WebPlatform
{
    public class MvcApplication : NinjectHttpApplication
    {
        //protected new void Application_Start()
        //{
        //    AreaRegistration.RegisterAllAreas();
        //    FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        //    RouteConfig.RegisterRoutes(RouteTable.Routes);
        //    BundleConfig.RegisterBundles(BundleTable.Bundles);

        //    MapperInit();

        //}

        protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();

            //Database.SetInitializer<ApplicationDbContext>(null);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            MapperInit();
        }

        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            RegisterServices(kernel);
            return kernel;
        }

        private void MapperInit()
        {

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Resource, ResourceModel>();
                cfg.CreateMap<ResourceModel, Resource>();

                cfg.CreateMap<ResourceModel, ResourceViewModel>();
                cfg.CreateMap<ResourceViewModel, ResourceModel>();

                cfg.CreateMap<MenuItem, MenuItemModel>();
                cfg.CreateMap<MenuItemModel, MenuItem>();

                cfg.CreateMap<ApplicationUser, UserViewModel>();
                cfg.CreateMap<UserViewModel, ApplicationUser>();

                cfg.CreateMap<ApplicationUser, UserEditViewModel>();
                cfg.CreateMap<UserEditViewModel, ApplicationUser>();

                cfg.CreateMap<ApplicationUser, UserModel>();
                cfg.CreateMap<UserModel, ApplicationUser>();

                cfg.CreateMap<Role, RoleModel>();
                cfg.CreateMap<RoleModel, Role>();

                cfg.CreateMap<RoleModel, RoleViewModel>();
                cfg.CreateMap<RoleViewModel, RoleModel>();

                cfg.CreateMap<RoleResourceModel, RoleResourceViewModel>();
                cfg.CreateMap<RoleResourceViewModel, RoleResourceModel>();

                cfg.CreateMap<RoleResource, RoleResourceModel>();
                cfg.CreateMap<RoleResourceModel, RoleResource>();

                cfg.CreateMap<UserRole, UserRoleModel>().ForMember(urm => urm.UserGuid, ur => ur.MapFrom(e => e.UserId) );
                cfg.CreateMap<UserRoleModel, UserRole>().ForMember(ur => ur.UserId, urm => urm.MapFrom(e => e.UserGuid)); 

                cfg.CreateMap<UserRoleModel, UserRoleViewModel>();
                cfg.CreateMap<UserRoleViewModel, UserRoleModel>();

            });
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private void RegisterServices(IKernel kernel)
        {
            // e.g. kernel.Load(Assembly.GetExecutingAssembly());
            //var context = new ApplicationDbContext();
            //kernel.Bind<DbContext>().ToConstant(context).InRequestScope();
            kernel.Bind<DbContext>().To<ApplicationDbContext>().InRequestScope();
            kernel.Bind<ResourceService>().ToSelf().InRequestScope();
            kernel.Bind<IMenuItemService>().To<MenuItemService>().InRequestScope();
            kernel.Bind<UserService>().ToSelf().InRequestScope();
            kernel.Bind<RoleService>().ToSelf().InRequestScope();
            kernel.Bind<RoleResourceService>().ToSelf().InRequestScope();
        }
    }
}

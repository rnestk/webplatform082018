﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebPlatform.Models;
using WebPlatform.Models.Administration;
using WebPlatform.Services.Administration;
using WebPlatform.ViewModels.Administration;
using WebPlatform.CORE.Helpers;

namespace WebPlatform.Controllers
{
    public class ResourceController : BaseController
    {

        private ResourceService _service;

        public ResourceController(ResourceService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult> List()
        {
            var all = await _service.GetAllAsync();            
            ResourceViewModel vm = new ResourceViewModel();
            vm.HandleCollection(all, 10, "Area", "Controller", "Action");

            return View(vm);
        }


        // GET: Resource/Create
        public ActionResult Add()
        {
            return View(new ResourceViewModel());
        }

        // POST: Resource/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add([Bind(Include = "Id,Area,Controller,Action,DateCreated,DateModified,IsActive,IsRemoved,rowGuid,AuthorId,Description,Symbol")] ResourceViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var model = MapperHelper.ToModel<ResourceViewModel, ResourceModel>(vm);
                await _service.AddAsync(model);
                return Redirect("/zasob-lista");
            }

            return View(vm);
        }

        public async Task<ActionResult> Edit(string rowGuid)
        {
            if (string.IsNullOrEmpty(rowGuid)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ResourceModel model = await _service.GetByIdAsync(rowGuid);
            var vm = MapperHelper.ToViewModel<ResourceModel, ResourceViewModel>(model);
            return View(vm);
        }

      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Area,Controller,Action,DateCreated,DateModified,IsActive,IsRemoved,rowGuid,AuthorId,Description,Symbol")] ResourceViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var model = MapperHelper.ToModel<ResourceViewModel, ResourceModel>(vm);
                var result = await _service.UpdateAsync(model);
                return RedirectToAction("List");
            }
            return View(vm);
        }

        // GET: Resource/Delete/5
        public async Task<ActionResult> Delete(string rowGuid)
        {
            if (string.IsNullOrEmpty(rowGuid)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ResourceModel user = await _service.GetByIdAsync(rowGuid);
            var vm = MapperHelper.ToViewModel<ResourceModel, ResourceViewModel>(user);
            return View(vm);
        }

        // POST: Resource/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string rowGuid)
        {
            if (string.IsNullOrEmpty(rowGuid)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ResourceModel resource = await _service.GetByIdAsync(rowGuid);
            await _service.DeleteAsync(resource);
            return RedirectToAction("List");
        }


        //public ActionResult AssignRole(string rowGuid)
        //{
        //    var vm = new ResourceViewModel();
        //    return PartialView(vm);
        //}


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> AssignRole([Bind(Include = "Description,Name,Area,Controller,Action,Id,DateCreated,DateModified,IsActive,IsRemoved,rowGuid,AuthorId")] ResourceViewModel vm)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var model = MapperHelper.ToModel<ResourceViewModel, ResourceModel>(vm);
        //        await _service.AddAsync(model);
        //        return Redirect("/role-zasoby-lista");
        //    }

        //    return PartialView(vm);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebPlatform.Models;
using WebPlatform.Services.Administration;
using WebPlatform.ViewModels.Administration.MenuItem;

namespace WebPlatform.Controllers
{
    public class MenuController : BaseController
    {
        private IMenuItemService _service;

        public MenuController(IMenuItemService service)
        {
            _service = service;
        }

        public MenuController()
        {
            _service = new MenuItemService(new ApplicationDbContext());
        }

        [ChildActionOnly]
        public ActionResult ListItems()
        {
            var all =  _service.GetAllForModel();
            var vm = new MenuItemViewModel();
            vm.Elements = all;
            return PartialView("_carouselRibbonPartial", vm);
        }

        public ActionResult ChangeOpen()
        {
            Session["MenuStateOpen"] = !(bool)Session["MenuStateOpen"];
            bool res = (bool)Session["MenuStateOpen"];
            return Json("OK", JsonRequestBehavior.AllowGet);
        }
    }
}
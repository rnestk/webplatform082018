﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebPlatform.Models;
using WebPlatform.Models.Administration;
using WebPlatform.Services.Administration;
using WebPlatform.ViewModels.Administration;
using WebPlatform.CORE.Helpers;
using WebPlatform.Services;
using WebPlatform.Services.Administration.User;

namespace WebPlatform.Controllers
{
    public class UserRoleController : BaseController
    {

        private UserRoleService _service;
        private RoleService _roleService;
        private UserService _userService;

        public UserRoleController(UserRoleService service, RoleService roleService, UserService userService)
        {
            _service = service;
            _roleService = roleService;
            _userService = userService;
        }

        public async Task<ActionResult> AssignRole(string userGuid)
        {
            if (string.IsNullOrEmpty(userGuid)) return HttpNotFound();
            var vm = new UserRoleViewModel();
            var roles = await _roleService.GetAllAsync();
            var user = await _userService.GetByIdAsync(userGuid);
            var junctionList = await _service.GetAllByUserIdAsync(user.Id);

            IList<UserRoleModel> list = _service.GenerateRolesAssignment(roles, junctionList, user.Id);
           
            vm.Elements = list;
            vm.UserGuid = user.RowGuid;
            vm.UserName = user.UserName;
            return PartialView(vm);
        }

       

        [HttpPost]
        public async Task<ActionResult> AssignChange(Guid roleGuid, Guid userGuid)
        {
            var role = await _roleService.GetByGuidAsync(roleGuid.ToString());
            var user = await _userService.GetByIdAsync(userGuid.ToString());
            if (null == role || null == user) return HttpNotFound();
            await _service.AddOrDeleteAsync(new UserRoleModel { RoleId = role.Id, UserGuid = user.Id });
            return RedirectToAction("AssignRole", new { userGuid = user.RowGuid.ToString() } );
        }

        

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _service = null;
            }
            base.Dispose(disposing);
        }
    }
}

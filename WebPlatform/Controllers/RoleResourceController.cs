﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebPlatform.Models;
using WebPlatform.Models.Administration;
using WebPlatform.Services.Administration;
using WebPlatform.ViewModels.Administration;
using WebPlatform.CORE.Helpers;
using WebPlatform.Services;

namespace WebPlatform.Controllers
{
    public class RoleResourceController : BaseController
    {

        private RoleResourceService _service;
        private ResourceService _resourceService;
        private RoleService _roleService;

        public RoleResourceController(RoleResourceService service, RoleService roleService, ResourceService resourceService)
        {
            _service = service;
            _resourceService = resourceService;
            _roleService = roleService;
        }

        public async Task<ActionResult> AssignRole(string resourceGuid)
        {
            if (string.IsNullOrEmpty(resourceGuid)) return HttpNotFound();
            var vm = new RoleResourceViewModel();
            var roles = await _roleService.GetAllAsync();
            var resource = await _resourceService.GetByGuidAsync(resourceGuid);
            var junctionList = await _service.GetAllByResourceId(resource.Id);

            IList<RoleResourceModel> list = _service.GenerateRolesAssignment(roles, junctionList, resource.Id);
           
            vm.Elements = list;
            vm.ResourceId = resource.Id;
            vm.ResourceGuid = resource.RowGuid;
            vm.ResorceName = resource.Description;
            return PartialView(vm);
        }

        [HttpPost]
        public async Task<ActionResult> AssignChange(Guid roleGuid, Guid resourceGuid)
        {
            var role = await _roleService.GetByGuidAsync(roleGuid.ToString());
            var resource = await _resourceService.GetByGuidAsync(resourceGuid.ToString());
            if (null == role || null == resource) return HttpNotFound();
            await _service.AddOrDeleteAsync(new RoleResourceModel { RoleId = role.Id, ResourceId = resource.Id });
            return RedirectToAction("AssignRole", new { resourceGuid = resource.RowGuid.ToString() } );
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _service = null;
            }
            base.Dispose(disposing);
        }
    }
}

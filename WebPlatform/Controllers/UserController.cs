﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebPlatform.Models;
using WebPlatform.ViewModels.Administration.User;
using WebPlatform.Services.Administration.User;
using WebPlatform.CORE.Helpers;

namespace WebPlatform.Controllers
{
    public class UserController : BaseController
    {
        private UserService _userService;

        public UserController(UserService service)
        {
            _userService = service;
        }

        public async Task<ActionResult> List()
        {
            var all = (await _userService.GetAllAsync()).ToList();
            UserViewModel vm = new UserViewModel();
            var users = MapperHelper.ToModelList<ApplicationUser, UserModel>(all);
            vm.HandleCollection(users, 10, "Email");
            //vm.ElementsTotalCount = users.Count;
            //vm.PageSize = 10;
            //vm.Elements = users;
            //vm.ha
            return View(vm);
        }

        public async Task<ActionResult> InActiveList()
        {
            var all = (await _userService.GetAllNonActiveAsync()).ToList();
            UserViewModel vm = new UserViewModel();
            var users = MapperHelper.ToModelList<ApplicationUser, UserModel>(all);
            vm.HandleCollection(users, 10, "Email");
            //vm.ElementsTotalCount = users.Count;
            //vm.PageSize = 10;
            //vm.Elements = users;
            //vm.ha
            return View(vm);
        }


        public ActionResult Add()
        {
            return View(new UserViewModel());
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add([Bind(Include = "Id,DateCreated,DateModified,IsActive,IsRemoved,RowGuid,AuthorId,Password,ConfirmPassword,UserName,Email")] UserViewModel vm)
        {
            var user = MapperHelper.ToEntity<ApplicationUser, UserViewModel>(vm);
            var result = await _userService.AddAsync(user, vm.Password);
            if(!result.Succeeded)
            {
                if(result.Errors.Any())
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }
            if (ModelState.IsValid)
            {
                return RedirectToAction("List");
            }

            return View(vm);
        }

        public async Task<ActionResult> Edit(string rowGuid)
        {
            if (string.IsNullOrEmpty(rowGuid)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ApplicationUser user = await _userService.GetByIdAsync(rowGuid);
            var vm = MapperHelper.ToViewModel<ApplicationUser, UserEditViewModel>(user);
            return View(vm);
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,DateCreated,DateModified,IsActive,IsRemoved,RowGuid,AuthorId,Email,UserName")] UserEditViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var user = MapperHelper.ToEntity<ApplicationUser, UserEditViewModel>(vm);
                var result = await _userService.UpdateAsync(user);
                return RedirectToAction("List");
            }
            return View(vm);
        }

        // GET: User/Delete/5
        public async Task<ActionResult> Delete(string rowGuid)
        {
            if (string.IsNullOrEmpty(rowGuid)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ApplicationUser user = await _userService.GetByIdAsync(rowGuid);
            var vm = MapperHelper.ToViewModel<ApplicationUser, UserViewModel>(user);
            return View(vm);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string rowGuid)
        {
            if (string.IsNullOrEmpty(rowGuid)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ApplicationUser user = await _userService.GetByIdAsync(rowGuid);
            await _userService.DeleteAsync(user);
            return RedirectToAction("List");
        }

        public async Task<ActionResult> Deactivate(string rowGuid)
        {
            if (string.IsNullOrEmpty(rowGuid)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ApplicationUser user = await _userService.GetByIdAsync(rowGuid);
            var vm = MapperHelper.ToViewModel<ApplicationUser, UserViewModel>(user);
            return View(vm);
        }

        [HttpPost, ActionName("Deactivate")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeactivateConfirmed(string rowGuid)
        {
            if (string.IsNullOrEmpty(rowGuid)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ApplicationUser user = await _userService.GetByIdAsync(rowGuid);
            await _userService.DeactivateAsync(user);
            return RedirectToAction("List");
        }

        public async Task<ActionResult> Activate(string rowGuid)
        {
            if (string.IsNullOrEmpty(rowGuid)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ApplicationUser user = await _userService.GetByIdAsync(rowGuid);
            var vm = MapperHelper.ToViewModel<ApplicationUser, UserViewModel>(user);
            return View(vm);
        }

        [HttpPost, ActionName("Activate")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ActivateConfirmed(string rowGuid)
        {
            if (string.IsNullOrEmpty(rowGuid)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            ApplicationUser user = await _userService.GetByIdAsync(rowGuid);
            await _userService.ActivateAsync(user);
            return RedirectToAction("List");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

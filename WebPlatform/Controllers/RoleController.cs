﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebPlatform.Models;
using WebPlatform.Models.Administration;
using WebPlatform.Services.Administration;
using WebPlatform.ViewModels.Administration;
using WebPlatform.CORE.Helpers;

namespace WebPlatform.Controllers
{
    public class RoleController : BaseController
    {

        private RoleService _service;

        public RoleController(RoleService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult> List()
        {
            var all = await _service.GetAllAsync();
            RoleViewModel vm = new RoleViewModel();
            vm.HandleCollection(all, 10, "Name", "Description");

            return View(vm);
        }


        // GET: Resource/Create
        public ActionResult Add()
        {
            return View(new RoleViewModel());
        }

        // POST: Resource/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add([Bind(Include = "Description,Name,Id,DateCreated,DateModified,IsActive,IsRemoved,rowGuid,AuthorId")] RoleViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var model = MapperHelper.ToModel<RoleViewModel, RoleModel>(vm);
                await _service.AddAsync(model);
                return Redirect("/role-lista");
            }

            return View(vm);
        }

        public async Task<ActionResult> Edit(string rowGuid)
        {
            if (string.IsNullOrEmpty(rowGuid)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            RoleModel model = await _service.GetByIdAsync(rowGuid);
            var vm = MapperHelper.ToViewModel<RoleModel, RoleViewModel>(model);
            return View(vm);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Description,Name,Id,DateCreated,DateModified,IsActive,IsRemoved,rowGuid,AuthorId")] RoleViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var model = MapperHelper.ToModel<RoleViewModel, RoleModel>(vm);
                var result = await _service.UpdateAsync(model);
                return RedirectToAction("List");
            }
            return View(vm);
        }

        // GET: Resource/Delete/5
        public async Task<ActionResult> Delete(string rowGuid)
        {
            if (string.IsNullOrEmpty(rowGuid)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            RoleModel user = await _service.GetByIdAsync(rowGuid);
            var vm = MapperHelper.ToViewModel<RoleModel, RoleViewModel>(user);
            return View(vm);
        }

        // POST: Resource/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string rowGuid)
        {
            if (string.IsNullOrEmpty(rowGuid)) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            RoleModel resource = await _service.GetByIdAsync(rowGuid);
            await _service.DeleteAsync(resource);
            return RedirectToAction("List");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _service = null;
            }
            base.Dispose(disposing);
        }
    }
}

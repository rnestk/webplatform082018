﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebPlatform.CORE.Helpers;
using WebPlatform.Models;
using WebPlatform.Models.Administration;
using WebPlatform.Services;
using WebPlatform.Services.Administration;
using WebPlatform.Services.Administration.User;
using WebPlatform.ViewModels.Administration;

namespace WebPlatform
{
    public class BaseController : Controller
    {
        private RoleResourceService _roleResourceService;
        private ResourceService _resourceService;
        private RoleService _roleService;
        private UserRoleService _userRoleService;
        private UserService _userService;

        public BaseController()
        {
            var ctx = new ApplicationDbContext();
            _roleResourceService = new RoleResourceService(ctx);
            _resourceService = new ResourceService(ctx);
            _roleService = new RoleService(ctx);
            _userRoleService = new UserRoleService(ctx);
            _userService = new UserService(ctx);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            bool isAdmin = CheckAdmin();
            if(!isAdmin)
            {
                ApplyAuth(filterContext);
            }
           

        }

        private bool CheckAdmin()
        {
            if (User.Identity == null) return false;
            if (User.Identity.GetUserId() == Guid.Empty.ToString()) return false;

            var administaratorRole = _roleService.GetAll().FirstOrDefault(e => e.Name.ToUpper() == "ADMINISTRATOR");
            var junction = _userRoleService.GetAllByUserId(User.Identity.GetUserId());
            if (junction.Any(e => e.RoleId == administaratorRole.Id)) return true;
        
            return false;
        }

        private void ApplyAuth(ActionExecutingContext filterContext)
        {
            bool isAuthorized = false;            
            string userGuid = User.Identity.GetUserId();
            var routingValues = filterContext.RouteData.Values;
            var currentArea = filterContext.RouteData.DataTokens["area"] ?? string.Empty;
            var currentController = (string)routingValues["controller"] ?? string.Empty;
            var currentAction = (string)routingValues["action"] ?? string.Empty;

            ResourceModel resource = null;

            if (string.IsNullOrEmpty(currentArea.ToString()))
            {
                resource = _resourceService.GetAll().FirstOrDefault(e => e.Controller == currentController && e.Action == currentAction);
            }
            else
            {
                resource = _resourceService.GetAll().FirstOrDefault(e => e.Controller == currentController && e.Action == currentAction && e.Area == currentArea.ToString());
            }

            if (null != resource)
            {

                if (!string.IsNullOrEmpty(userGuid))
                {
                    var roles = _userRoleService.GetAllByUserId(userGuid);
                    if (roles.Any())
                    {
                        var byResouce = _roleResourceService.GetAll().Where(e => e.ResourceId == resource.Id);
                        if (byResouce.Any())
                        {
                            var roleids = byResouce.Select(e => e.RoleId).ToList();
                            foreach (var roleid in roleids)
                            {
                                if (roles.Any(e => e.RoleId == roleid))
                                {
                                    isAuthorized = true;
                                    break;
                                }
                            }
                        }

                    }
                }
            }

            if (filterContext.IsChildAction) isAuthorized = true;

            if (!isAuthorized)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        {"controller", "Unauthorized"},
                        {"action", "Index"}
                    }
                );
            }
        }
    }
}
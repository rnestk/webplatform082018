﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebPlatform.Models;
using WebPlatform.Models.Administration;
using WebPlatform.Services.Administration;
using WebPlatform.Services.Administration.User;
using WebPlatform.ViewModels.Administration.User;

namespace WebPlatform.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            //var ctx = new ApplicationDbContext();
            //var userService = new UserService(ctx);
            //var roleService = new RoleService(ctx);

            
            //var roles = roleService.GetAll();

            //ApplicationUser user = new ApplicationUser();
            //user.UserName = "jan.kowalski2@op.pl";
            //user.IsActive = true;
            //user.Email = "jan.kowalski2@op.pl";
            //user.DateCreated = DateTime.Now;
            //user.AuthorId = Guid.NewGuid().ToString();
            //user.IsRemoved = false;

            //var idresult = userService.Add(user, "Test123!");

            //var users = userService.GetAll();
            //var user2 = users.Where(u => u.UserName == "jan.kowalski2@op.pl").FirstOrDefault();

            //Role role = new Role
            //{
            //    AuthorId = Guid.NewGuid().ToString(),
            //    IsActive = true,
            //    DateCreated = DateTime.Now,
            //    Description = null,
            //    IsRemoved = false,
            //    Name = "Użytkownik",
            //    RowGuid = Guid.NewGuid().ToString()
            //};
            //role.Users.Add(user2);

            //int result = roleService.Add(role);

            return Content("") ;
        }


        public async Task<ActionResult> TestPopup()
        {

            return View();
        }

        public async Task<ActionResult> TestAction()
        {
            var list = new List<string>
            {
                "Karolina", "Klaudia", "Kamila"
            };
            return View(list);
        }

        public async Task<ActionResult> TestService()
        {
            ResourceService rs = new ResourceService(new ApplicationDbContext());
            var all = rs.GetAll();

            return Content("test service");
        }

        public async Task<ActionResult> TestAutocomplete()
        {
            UserViewModel model = new UserViewModel();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> TestAutocomplete(UserViewModel vm)
        {
            return View(vm);
        }

        public async Task<ActionResult> TestAutocompleteJson(string term)
        {
            ApplicationDbContext ctx = new ApplicationDbContext();
            UserService us = new UserService(ctx);
            var users = (await us.GetAllAsync())
                .Where(u => u.UserName.ToLower()
                .Contains(term.ToLower()))
                .OrderBy(e => e.UserName)
                .Take(10)
                .ToList();
            var json = users.Select(u => new SelectListItem { Text = string.Format("{1} {0}", u.Id, u.UserName), Value = u.Id }).ToList();
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> TestAutocompleteJson2(string term)
        {
            ApplicationDbContext ctx = new ApplicationDbContext();
            UserService us = new UserService(ctx);
            var users = (await us.GetAllAsync())
                .Where(u => u.UserName.ToLower()
                .Contains(term.ToLower()))
                .OrderBy(e => e.UserName)
                .Take(10)
                .ToList();
            var json = users.Select(u => new SelectListItem { Text = string.Format("{1} {0}", u.Id, u.UserName), Value = u.Id  }).ToList();
            return Json(json, JsonRequestBehavior.AllowGet);
        }

    }
}
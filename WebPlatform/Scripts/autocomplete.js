﻿
"use strict";

$(document).ready(function () {


    $("input[data-autocomplete]").each(function (i) {
        var url = $(this).attr("data-autocomplete");
        var width = $(this).attr("data-width");
        var defaultWidth = 250;
        $(this).autocomplete({
            source: url,
            minLength: 2,
            _resizeMenu: function () {

            },
            _renderItem: function (ul, item) {
                console.log(item);
            },
            create: function (event, ui) {
                //console.log("create");
            },
            focus: function (event, ui) {
                //console.log("focus");
                // zmiana tła elementu listy dynamicznie dodawanej - działa razem z css w autocomplete.css
                $(".ui-menu-item .ui-menu-item-wrapper").css("background", "none");
                $(this).val(ui.item.Text);
                return false;
            },
            open: function (event, ui) {
                console.log("open");
                // ustawienie szerokości dynamicznie dodawanej listy do autocomplete
                $("ul.ui-menu").css("width", (width === undefined || width === null ? defaultWidth : width));
            },
            select: function (event, ui) {
                console.log("select");
                var selected = ui.item.Value;
                console.log(selected);
                console.log(ui.item.Text);
                console.log(event);
                $(this).val(ui.item.Text);
                var id = $(this).attr("id");
                var fieldId = id.split("_");
                $("#" + fieldId[0]).val(ui.item.Value);
                return false;
            },
            change: function (event, ui) {
               // console.log("change");
            },
            search: function (event, ui) {
                //console.log("search");
            },
            response: function (event, ui) {
                console.log("response");
                console.log(ui);
            },
            close: function (event, ui) {
                //console.log("close");
            }

        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
                .append("<div>" + item.Text + "</div>")
                .appendTo(ul);
        };
    });


});
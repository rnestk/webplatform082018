﻿

$(document).ready(function () {



    // ACCORDION CONFIG
    $(".accordion").accordion({
        collapsible: true,
        heightStyle: "content"
    });

    if (!accordionState.open) {
        $(".accordion .accordion-fix").click();
    }



    // SLICK CAROUSEL CONFIG

    $('.ribbon-carousel-slick').slick({
        dots: false,
        infinite: false,
        speed: 200,
        slidesToShow: 4,
        slidesToScroll: 4,
        prevArrow: "#prevSlickArrow",
        nextArrow: "#nextSlickArrow",
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                }
            }

        ]
    });

    window.setTimeout(function () {
        //$(".ribbon-carousel-slick").css("visibility", "visible");
        $('.ribbon-carousel-slick').css({ opacity: 0, visibility: "visible" }).animate({ opacity: 1.0 }, 1000);
    }, 500);

    // MENU CONFIG

    $(".trigger").click(function (ev) {
        ev.preventDefault();
        var dataid = $(this).attr("data-trigger");
        //console.log(dataid);
        var fa = $("i[data-ico='" + dataid + "']");
        var list = $("ul[data-trigger='" + dataid + "']");
        if ($(list).hasClass("element-hidden")) {
            $(list).removeClass("element-hidden");
            $(fa).removeClass("fa-caret-down");
            $(fa).addClass("fa-caret-up");
        }
        else {
            $(list).addClass("element-hidden");
            $(fa).removeClass("fa-caret-up");
            $(fa).addClass("fa-caret-down");
        }
    });

});

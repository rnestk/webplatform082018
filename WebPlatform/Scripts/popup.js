﻿"use strict";

function AppendActionResult(url, element) {
    //console.log($(element));
    //console.log(url);
    $.ajax({
        url: url,
        success: function (data) {
            //console.log(data);
            $(element).html(data);
        }
    });
}


function ClosePopupSilently() {
    $('div[role="dialog"]').modal("hide");
}

$(document).ready(function () {

    $('div[role="dialog"]').modal({
        backdrop: 'static',
        show: false,
        focus: true
    })

    $('div[role="dialog"]').on('show.bs.modal', function (e) {
        //console.log('show.bs.modal');
        
        var trigger = $(e.relatedTarget);
        var url = trigger.attr("data-url");
        //console.log(trigger);
        //console.log(url);
        AppendActionResult(url, "div[data-container='append']");
    });

    $('div[role="dialog"]').on('shown.bs.modal', function (e) {
        //console.log('shown.bs.modal');
    });

    $('div[role="dialog"]').on('hide.bs.modal', function (e) {
        //console.log('hidde.bs.modal');
    });

    $('div[role="dialog"]').on('hidden.bs.modal', function (e) {
        //console.log('hidden.bs.modal');
    });

});
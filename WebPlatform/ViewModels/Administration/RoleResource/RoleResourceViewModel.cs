﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPlatform.CORE;

namespace WebPlatform.ViewModels.Administration
{
    public class RoleResourceViewModel : BaseViewModel<RoleResourceModel>
    {
        public string ResorceName { get; set; }

        public bool IsAssigned { get; set; }
        public int ResourceId { get; set; }
        public string ResourceGuid { get; set; }
        public string RoleName { get; set; }

    }
}
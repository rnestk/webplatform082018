﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPlatform.CORE;

namespace WebPlatform.ViewModels.Administration
{
    public class RoleResourceModel : BaseModel
    {
        public int RoleId { get; set; }
        public int ResourceId { get; set; }
        public bool IsAssigned { get; set; }
        public string RoleName { get; set; }
        public string RoleGuid { get; set; }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPlatform.CORE;

namespace WebPlatform.ViewModels.Administration
{
    public class UserRoleModel : BaseModel
    {
        public int RoleId { get; set; }
        public string UserGuid { get; set; }
        public bool IsAssigned { get; set; }
        public string RoleName { get; set; }
        public string RoleGuid { get; set; }
        public string UserName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPlatform.CORE;

namespace WebPlatform.ViewModels.Administration
{
    public class RoleModel : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string DisplayName { get; set; }
        public bool IsAssign { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebPlatform.CORE;

namespace WebPlatform.ViewModels.Administration
{
    public class RoleViewModel : BaseViewModel<RoleModel>
    {
        [Display(Name = "Nazwa roli")]
        [Required(ErrorMessage = "Nazwa roli jest wymagana")]
        public string Name { get; set; }

        [Display(Name = "Opis roli")]
        public string Description { get; set; }

        [Display(Name = "Nazwa wyświetlana roli")]
        [Required(ErrorMessage = "Nazwa wyświetlana roli jest wymagana")]
        public string DisplayName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPlatform.CORE;

namespace WebPlatform.ViewModels.Administration
{
    public class MenuItemModel : BaseModel
    {
        public int? ParentId { get; set; }
        public string ImageUrl { get; set; }
        public string RouteUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public IList<MenuItemModel> Items { get; set; }

        public MenuItemModel()
        {
            Items = new List<MenuItemModel>();
        }

    }
}
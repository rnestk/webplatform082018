﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebPlatform.CORE;
using WebPlatform.Models.Administration;

namespace WebPlatform.ViewModels.Administration
{
    public class ResourceViewModel : BaseViewModel<ResourceModel>
    {
        [Display(Name = "Obszar")]
        public string Area { get; set; }

        [Display(Name = "Kontroler")]
        [Required(ErrorMessage = "Nazwa kontrolera jest wymagana")]
        public string Controller { get; set; }

        [Display(Name = "Akcja")]
        [Required(ErrorMessage = "Nazwa akcji jest wymagana")]
        public string Action { get; set; }

        [Display(Name = "Nazwa opisowa")]
        [Required(ErrorMessage = "Nazwa opisowa jest wymagana")]
        public string Description { get; set; }

        [Display(Name = "Symbol zasobu")]
        [Required(ErrorMessage = "Symbol zasobu jest wymagany")]
        public string Symbol { get; set; }


    }
}
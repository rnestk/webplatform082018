﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPlatform.CORE;

namespace WebPlatform.ViewModels.Administration.User
{
    public class UserModel : BaseModel
    {
        public new string Id { get; set; }
        //public virtual ICollection<Role> AdmRoles { get; set; }
        public virtual string Email { get; set; }
        public virtual bool EmailConfirmed { get; set; }
        public virtual string PasswordHash { get; set; }
        public virtual string SecurityStamp { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual bool PhoneNumberConfirmed { get; set; }
        public virtual bool TwoFactorEnabled { get; set; }
        public virtual DateTime? LockoutEndDateUtc { get; set; }
        public virtual bool LockoutEnabled { get; set; }
        public virtual int AccessFailedCount { get; set; }
        //public virtual ICollection<TRole> Roles { get; }
        //public virtual ICollection<TClaim> Claims { get; }
        //public virtual ICollection<TLogin> Logins { get; }
        //public virtual TKey Id { get; set; }
        public virtual string UserName { get; set; }
        public string Password { get; set; }
    }
}
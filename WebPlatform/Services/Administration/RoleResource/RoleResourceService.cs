﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebPlatform.CORE;
using WebPlatform.CORE.Helpers;
using WebPlatform.Models.Administration;
using WebPlatform.Repository.Administration;
using WebPlatform.ViewModels.Administration;

namespace WebPlatform.Services
{
    public class RoleResourceService : BaseService<RoleResourceModel, RoleResource>
    {
        public RoleResourceService(DbContext context)
        {
            _dao = new RoleResourceRepository(context);
        }

        public override async Task<IList<RoleResourceModel>> GetAllAsync()
        {
            return await base.GetAllAsync();
        }

        public async Task<IList<RoleResourceModel>> GetAllByResourceId(int resourceId)
        {
            return (await base.GetAllAsync()).Where(rr => rr.ResourceId == resourceId).ToList();
        }

        public IList<RoleResourceModel> GenerateRolesAssignment(IList<RoleModel> roles, IList<RoleResourceModel> junctionList, int resourceId)
        {
            var roleModelList = new List<RoleResourceModel>();
            if (null == roles || !roles.Any()) return roleModelList;
            foreach (var role in roles)
            {
                var any = junctionList.Where(e => e.RoleId == role.Id).FirstOrDefault();
                var model = new RoleResourceModel { RoleId = role.Id, ResourceId = resourceId, IsAssigned = false, RoleName = role.Name, RoleGuid = role.RowGuid };
                if (null != any) model.IsAssigned = true;
                roleModelList.Add(model);
            }

            return roleModelList;
        }

        public async Task<int> AddOrDeleteAsync(RoleResourceModel model)
        {
            return await Task.Run(() => 
            {
                var entity = GetAll().FirstOrDefault(e => e.RoleId == model.RoleId && e.ResourceId == model.ResourceId);
                if(entity != null)
                {
                    return DeletePernament(model.ResourceId, model.RoleId);
                }
                else
                {
                  return Add(model);
                }
            });
        }

        public RoleResourceModel GetById(int resourceId, int roleId)
        {
            return GetAll().FirstOrDefault(e => e.ResourceId == resourceId && e.RoleId == roleId);
        }

        public int DeletePernament(int resourceId, int roleId)
        {
            //var model = GetById(resourceId, roleId);
            //var entity = MapperHelper.ToEntity<RoleResource, RoleResourceModel>(model);
            var entity = _dao.GetAll().FirstOrDefault(e => e.RoleId == roleId && e.ResourceId == resourceId);
            return _dao.DeletePernament(entity);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebPlatform.CORE;
using WebPlatform.CORE.Helpers;
using WebPlatform.Models.Administration;
using WebPlatform.Repository.Administration;
using WebPlatform.ViewModels.Administration;

namespace WebPlatform.Services
{
    public class UserRoleService : BaseService<UserRoleModel, UserRole>
    {
        public UserRoleService(DbContext context)
        {
            _dao = new UserRoleRepository(context);
        }

        public override async Task<IList<UserRoleModel>> GetAllAsync()
        {
            return await base.GetAllAsync();
        }

        public async Task<IList<UserRoleModel>> GetAllByUserIdAsync(string userGuid)
        {
            return (await base.GetAllAsync()).Where(rr => rr.UserGuid == userGuid).ToList();
        }

        public IList<UserRoleModel> GetAllByUserId(string userGuid)
        {
            return base.GetAll().Where(rr => rr.UserGuid == userGuid).ToList();
        }

        public IList<UserRoleModel> GenerateRolesAssignment(IList<RoleModel> roles, IList<UserRoleModel> junctionList, string userGuid)
        {
            var roleModelList = new List<UserRoleModel>();
            if (null == roles || !roles.Any()) return roleModelList;
            foreach (var role in roles)
            {
                var any = junctionList.Where(e => e.RoleId == role.Id).FirstOrDefault();
                var model = new UserRoleModel { RoleId = role.Id, UserGuid = userGuid, IsAssigned = false, RoleName = role.Name, RoleGuid = role.RowGuid };
                if (null != any) model.IsAssigned = true;
                roleModelList.Add(model);
            }

            return roleModelList;
        }

        public async Task<int> AddOrDeleteAsync(UserRoleModel model)
        {
            return await Task.Run(() => 
            {
                var entity = GetAll().FirstOrDefault(e => e.RoleId == model.RoleId && e.UserGuid == model.UserGuid);
                if(entity != null)
                {
                    return DeletePernament(model.UserGuid, model.RoleId);
                }
                else
                {
                  return Add(model);
                }
            });
        }

        public UserRoleModel GetById(string userGuid, int roleId)
        {
            return GetAll().FirstOrDefault(e => e.UserGuid == userGuid && e.RoleId == roleId);
        }

        public int DeletePernament(string userGuid, int roleId)
        {
           
            var entity = _dao.GetAll().FirstOrDefault(e => e.RoleId == roleId && e.UserId == userGuid);
            return _dao.DeletePernament(entity);
        }

    }
}
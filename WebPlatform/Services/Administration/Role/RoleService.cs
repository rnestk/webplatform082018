﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebPlatform.CORE;
using WebPlatform.Dao.Administration;
using WebPlatform.Models.Administration;
using WebPlatform.ViewModels.Administration;

namespace WebPlatform.Services.Administration
{
    public class RoleService : BaseService<RoleModel, Role>
    {
        public RoleService(DbContext context)
        {
            _dao = new RoleRepository(context);
        }
    }
}
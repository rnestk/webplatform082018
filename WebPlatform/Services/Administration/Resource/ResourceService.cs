﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebPlatform.CORE;
using WebPlatform.Dao.Administration;
using WebPlatform.Models.Administration;
using WebPlatform.ViewModels.Administration;

namespace WebPlatform.Services.Administration
{
    public class ResourceService : BaseService<ResourceModel, Resource>
    {
        public ResourceService(DbContext context)
        {
            _dao = new Resourcerepository(context);
        }
    }
}
﻿using System.Collections.Generic;
using WebPlatform.ViewModels.Administration;

namespace WebPlatform.Services.Administration
{
    public interface IMenuItemService
    {
        IList<MenuItemModel> GetAllForModel();
    }
}
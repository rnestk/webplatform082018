﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebPlatform.CORE;
using WebPlatform.CORE.Helpers;
using WebPlatform.Dao.Administration;
using WebPlatform.Models.Administration;
using WebPlatform.ViewModels.Administration;

namespace WebPlatform.Services.Administration
{
    public class MenuItemService : BaseService<MenuItemModel, MenuItem>, IMenuItemService
    {
        public MenuItemService(DbContext context)
        {
            _dao = new MenuItemRepository(context);
        }
        
        public IList<MenuItemModel> GetAllForModel()
        {
            var all =  base.GetAll();
            var list = new List<MenuItemModel>();

            if (all.Any())
            {
                var boxList = (from mi in all
                            where mi.ParentId == null
                            select mi).ToList();

                foreach (var item in boxList)
                {
                    var items = (from mi in all
                                 where mi.ParentId == item.Id
                                 select mi).ToList();
                    if (!items.Any()) continue;
                    item.Items = items;
                    list.Add(item);
                    GetItems(items, all, list);
                }
                
            }

            return list;

        }

        private void GetItems(IList<MenuItemModel> boxList, IList<MenuItemModel> all, IList<MenuItemModel> outputList)
        {
            foreach (var item in boxList)
            {
                var items = (from mi in all
                             where mi.ParentId == item.Id
                             select mi).ToList();
                if (!items.Any()) continue;
                item.Items = items;
                GetItems(items, all, outputList);
            }
        }

        
    }
}
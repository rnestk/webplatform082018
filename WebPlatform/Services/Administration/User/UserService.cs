﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebPlatform.CORE;
using WebPlatform.Dao.Administration;
using WebPlatform.Models;

namespace WebPlatform.Services.Administration.User
{
    
    public class UserService 
    {
        protected UserRepository _dao;
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public UserService(DbContext context)
        {
            context.Configuration.ProxyCreationEnabled = false;
            _dao = new UserRepository(context);
        }

        public IQueryable<ApplicationUser> GetAll()
        {
            return _dao.GetAll();
        }

        public IQueryable<ApplicationUser> GetAllWithNonActive()
        {
            return _dao.GetAllWithNonFiltered();
        }

        public IQueryable<ApplicationUser> GetAllNonActive()
        {
            return _dao.GetAllWithNonFiltered().Where(e => !e.IsActive);
        }

        public async Task<IQueryable<ApplicationUser>> GetAllNonActiveAsync()
        {
            return await Task.Run(() => 
            {
                return _dao.GetAllWithNonFiltered().Where(e => !e.IsActive);
            });
        }

        public async Task<IQueryable<ApplicationUser>> GetAllAsync()
        {
            return await _dao.GetAllAsync();
        }

        public ApplicationUser GetById(string id)
        {
            return _dao.GetById(id);
        }

        public async Task<ApplicationUser> GetByIdAsync(string id)
        {
            return await _dao.GetByIdAsync(id);
        }

        public async Task<IdentityResult> AddAsync(ApplicationUser user, string password)
        {
            IdentityResult result;
            Guid userId = Guid.NewGuid();
            try
            {
                user.Id = userId.ToString();
                user.DateCreated = DateTime.Now;
                user.IsActive = true;
                user.IsRemoved = false;
                user.RowGuid = userId.ToString();
                
                result = await UserManager.CreateAsync(user, password);
            }
            catch(DbEntityValidationException e)
            {
                result = null;
                throw;
            }
            
            return result;
        }

        public IdentityResult Add(ApplicationUser user, string password)
        {
            IdentityResult result;
            Guid userId = Guid.NewGuid();
            try
            {
                user.Id = userId.ToString();
                user.DateCreated = DateTime.Now;
                user.IsActive = true;
                user.IsRemoved = false;
                user.RowGuid = userId.ToString();

                result = UserManager.Create(user, password);
            }
            catch (DbEntityValidationException e)
            {
                result = null;
                throw;
            }

            return result;
        }

        public IdentityResult Update(ApplicationUser user)
        {
            ApplicationUser original = UserManager.Users.FirstOrDefault(u => u.RowGuid == user.RowGuid);
            original.DateModified = DateTime.Now;
            original.UserName = user.UserName;
            original.Email = user.Email;
            original.IsRemoved = user.IsRemoved;
            return UserManager.Update(original);
        }

        public async Task<IdentityResult> UpdateAsync(ApplicationUser user)
        {
            ApplicationUser original = await UserManager.Users.FirstOrDefaultAsync(u => u.RowGuid == user.RowGuid);
            original.DateModified = DateTime.Now;
            original.UserName = user.UserName;
            original.Email = user.Email;
            original.IsRemoved = user.IsRemoved;
            return await UserManager.UpdateAsync(original);
        }

        public async Task<IdentityResult> DeleteAsync(ApplicationUser user)
        {
            user.IsRemoved = true;
            user.DateModified = DateTime.Now;
            await UserManager.SetLockoutEnabledAsync(user.Id, true);
            return await UpdateAsync(user);
        }

        public IdentityResult Delete(ApplicationUser user)
        {
            user.IsRemoved = true;
            user.DateModified = DateTime.Now;
            UserManager.SetLockoutEnabled(user.Id, true);
            return Update(user);
        }

        public async Task<IdentityResult> DeactivateAsync(ApplicationUser user)
        {
            return await Task.Run(() => 
            {

                user.IsRemoved = false;
                user.IsActive = false;
                user.DateModified = DateTime.Now;
                return UpdateAsync(user);
            });
           
        }

        public IdentityResult Deactivate(ApplicationUser user)
        {
            user.IsRemoved = false;
            user.IsActive = false;
            user.DateModified = DateTime.Now;
            UserManager.SetLockoutEnabled(user.Id, true);
            return Update(user);
        }

        public IdentityResult Activate(ApplicationUser user)
        {
            user.IsRemoved = false;
            user.IsActive = true;
            user.DateModified = DateTime.Now;
            UserManager.SetLockoutEnabled(user.Id, true);
            return Update(user);
        }

        public async Task<IdentityResult> ActivateAsync(ApplicationUser user)
        {
            return await Task.Run(() =>
            {

                user.IsRemoved = false;
                user.IsActive = true;
                user.DateModified = DateTime.Now;
                return UpdateAsync(user);
            });

        }

        public IdentityResult DeleteFinal(ApplicationUser user)
        {
            return UserManager.Delete(user);
        }

        public async Task<IdentityResult> DeleteFinalAsync(ApplicationUser user)
        {
            return await UserManager.DeleteAsync(user);
        }

    }
}
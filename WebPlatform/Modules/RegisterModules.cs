﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[assembly: PreApplicationStartMethod(typeof(WebPlatform.Modules.RegisterModules),"Register")]
namespace WebPlatform.Modules
{
    public class RegisterModules
    {
        public static void Register()
        {
            HttpApplication.RegisterModule(typeof(WebPlatform.Modules.AuthorizationModule));
        }
    }
}
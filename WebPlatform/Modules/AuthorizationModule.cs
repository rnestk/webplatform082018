﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebPlatform.Modules
{
    public class AuthorizationModule : IHttpModule
    {
        public void Dispose()
        {
            
        }

        public void Init(HttpApplication app)
        {
            app.MapRequestHandler += (o, ev) => 
            {
                RouteValueDictionary rvdict = app.Context.Request.RequestContext.RouteData.Values;
                string controller = rvdict["controller"].ToString();
                string action = rvdict["action"].ToString();
                var user = app.Context.User;

                if (!Compare(rvdict, "controller", "unauthorized"))
                {
                    string url = UrlHelper.GenerateUrl("", "Index", "Unauthorized", rvdict, RouteTable.Routes, app.Context.Request.RequestContext, false);
                    //app.Context.Response.Redirect(url);
                }
                
               
            };

        }

        private bool Compare(RouteValueDictionary rvd, string key, string value)
        {
            return string.Equals((string)rvd[key], value, StringComparison.OrdinalIgnoreCase);
        }
    }
}